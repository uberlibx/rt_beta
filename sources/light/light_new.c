#include "rt.h"

t_light		light_new(double *params)
{
	t_light		light;

	light.type = (int)params[0];
	light.id = (int)params[1];
	light.cast_shadow = (int)params[12];
	light.ls = params[11];
	light.color = color_new(params[8], params[9], params[10]);
	light.location = p3d_new(params[2], params[3], params[4]);
	light.v_rotate = p3d_new(params[5], params[6], params[7]);
	matrix_identity(&light.tr_rotate);
	tr_rotate(&light.tr_rotate, params[5], params[6], params[7]);
	light.dir = matrix_mult_dir(light.tr_rotate, dir_new(0, 1, 0));
//	printf("dir: x: %f  y: %f  z: %f\n", light.dir.x, light.dir.y, light.dir.z);
	return (light);
}
