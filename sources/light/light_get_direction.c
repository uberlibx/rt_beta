#include "rt.h"

t_dir			light_get_direction(t_light *light, t_shaderec *sr)
{
	t_dir	dir;

	dir = dir_new(0 , 0, 0);
	if (light->type == -1)
	{
		dir = light->dir;
	}
	else if (light->type == -2)
	{
		dir = dir_btwn_2_p3d(light->location, sr->hit_point);
		dir_normalize(&dir);
	}
	return (dir);
}
