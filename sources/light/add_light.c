#include "rt.h"

void	add_light(t_world *world, double *params)
{
	t_light	light;
	t_llist	*ltemp;

	light = light_new(params);
	ltemp = world->light_list;
	while (ltemp && ltemp->next)
		ltemp = ltemp->next;
	if (world->light_list)
	{
		ltemp->next = (t_llist*)malloc(sizeof(t_llist));
		ltemp->next->light = light;
		ltemp->next->next = NULL;
	}
	else
	{
		world->light_list = (t_llist*)malloc(sizeof(t_llist));
		world->light_list->light = light;
		world->light_list->next = NULL;
	}
}
