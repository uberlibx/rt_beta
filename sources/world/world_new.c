#include "rt.h"

/*
 * Called from data_init
 * t_world has sampler (tutorial - viewport).. also there is no viewport
 */

t_world			world_new()
{
	t_world		world;

	//world.eye = p3d_new(0, 0, EYE_Z); // chapter 8
	//world->background_color = color_new(242, 242, 242);
	world.background_color = color_new(0, 0, 0);
	world.sampler = sampler_new(SETS, SAMPLES, 4); // 4 - mjit
	world.camera = camera_new(p3d_new(0, 0, EYE_Z), p3d_new(0, 0, 0), DIST_VP);
//	world.ambient = light_new_amb(color_new(255, 255, 255));
	world.obj_list = NULL;
	world.light_list = NULL;
	world.selected_ob = NULL;
	world.selected_light = NULL;
	world.default_materials = NULL;
	world.custom_materials = NULL;
	return (world);
}
