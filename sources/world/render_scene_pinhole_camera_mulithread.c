#include "rt.h"

/*
 * Perspective view, pinhole camera, with multisampling
 */

void			*render_scene_pinhole_camera_multithread(void *thread_void)
{
	///*
	t_color		L;
	t_ray		ray;
	int			r, c;
	int			j;
	t_p2d		sp; // sample point from sampler in range of [0, 1] x [0, 1]
	t_p2d		pp; // the point around current pixl on viewplane
	double		psize; // pixel size
	t_thread	*thread;
	t_data		*data;

	thread = (t_thread*)thread_void;
	data = thread->data;

	if (thread->nr == 0) {
		//ft_putstr("		render ");
		//ft_putnbr(thread->nr);
		//ft_putendl(" : START");
	}

	compute_uvw(&data->world.camera);

	if (thread->nr == 0) {
		//ft_putendl("			compute_uvw: OK");
	}

	psize = PIXEL_SIZE / data->world.camera.zoom;
	ray.origin = data->world.camera.eye;

	r = thread->start_h - 1;
	while (++r < thread->end_h)
	{
		c = -1;
		while (++c < WIDTH)
		{
			//if (r % 2 == 0 || c % 2 == 0)
			{
				//L = data->world.background_color;
				//build_color(&L);
				//data->pixel[c + (HEIGHT - r - 1) * WIDTH] = L.color;
				//continue;
			}
			L = color_new(0, 0, 0);

			if (thread->nr == 0 && c == 0 && r == thread->start_h) {
				//ft_putendl("			samples: START");
			}

			j = -1;
			while (++j < SAMPLES)
			{
				sp = sample_unit_square(data->world.sampler);

				if (thread->nr == 0 && j == 0 && c == 0 && r == thread->start_h) {
					//ft_putendl("				sample_unit_square: OK");
				}

				pp.x = psize * (c - 0.5 * WIDTH + sp.x);
				pp.y = psize * (r - 0.5 * HEIGHT + sp.y);
				ray.dir = ray_direction(&data->world.camera, pp);

				if (thread->nr == 0 && j == 0 && c == 0 && r == thread->start_h) {
					//ft_putendl("				trace_ray: START");
				}

				L = color_add_color(L, trace_ray(&data->world, &ray));

				if (thread->nr == 0 && j == 0 && c == 0 && r == thread->start_h) {
					//ft_putendl("				trace_ray: OK");
				}

				if (thread->nr == 0 && j == 0 && c == 0 && r == thread->start_h) {
					//ft_putendl("				color: OK");
				}

			}

			if (thread->nr == 0 && c == 0 && r == thread->start_h) {
				//ft_putendl("			samples: OK");
			}

			L = color_div_scalar(L, SAMPLES);
			//L = color_clamp_to_color(L);
			L = color_max_to_one(L);

			build_color_raw(&L);
			//build_color(&L);
			data->pixel[c + (HEIGHT - r - 1) * WIDTH] = L.color;
		}
	}

	//mlx_put_image_to_window(data->mlx, data->win, data->img, 0, 0);
	//printf("thread %i done\n", thread->nr);

	if (thread->nr == 0) {
		//ft_putstr("		render ");
		//ft_putnbr(thread->nr);
		//ft_putendl(" : OK");
	}

	return (NULL);
}
