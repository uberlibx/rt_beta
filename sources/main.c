#include "rt.h"

void	error(int code)
{
	if (code == 0)
		ft_putendl("Error 0: Scene file could not be opened or created");
	else if (code == 1)
		ft_putendl("Error 1: Failed to read specified scene");
	else
		ft_putendl("Error code unknown, please contact developer");
	exit(1);
}

int			main(int ac, char **av)
{
	t_data	data;

	if (ac == 2)
	{
		//ft_putendl("init: START");
		data_init(&data);
	//	data.world.ambient.ls = 0.301;
		//ft_putendl("init: OK");
		//ft_putendl("load_scene: START");
		load_scene(&data, av[1]);
		//ft_putendl("load_scene: OK");

	//	mlx_hook(data.win, 2, 1, event_keyboard, &data);
		mlx_loop_hook(data.mlx, event_render, &data);
		mlx_loop(data.mlx);
	}
	return (0);
}
