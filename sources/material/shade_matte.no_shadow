#include "rt.h"

/*
** Matte material with SHADOWS
*/

t_color			shade_matte(t_shaderec *sr)
{
	t_color		color; // a.k.a. L
	t_dir		wo;
	t_dir		wi;
	float		ndotwi;
	int			num_lights;
	int			j;
	int			in_shadow;
	t_ray		sray;
	double		dist_to_light;
	double		dist_atenuation = 1.0;

	wo = dir_mult_scalar(sr->ray->dir, -1.0);
		
	color = color_mult_color(
		brdf_rho(sr->material->ambient, sr, wo),
		light_l(sr->world->ambient, sr)
	);


	num_lights = sr->world->light_arr_len;
	
	j = -1;
	while (++j < num_lights)
	{
		t_p3d	pt = sr->world->light_arr[j]->location;
		if (sr->world->light_arr[j]->ls > 0)
		{			
			dist_atenuation = 1;
			if (sr->world->light_arr[j]->type == 2)
			{
				dist_to_light = dist_btwn_2_p3d(sr->hit_point, sr->world->light_arr[j]->location);
				dist_atenuation = 1.0 - dist_to_light / 1000;
				if (dist_atenuation < 0.0)
					dist_atenuation = 0.0;
			}
			
			wi = light_get_direction(sr->world->light_arr[j], sr);

			ndotwi = nl_dot_dir(sr->normal, wi);
			if (ndotwi > 0)
			{
				in_shadow = 0;
				if (sr->world->light_arr[j]->cast_shadow == 1)
				{				
					sray = ray_new(sr->hit_point, wi);
					in_shadow = hit_shadow(sr->world, &sray, sr->world->light_arr[j], sr->ob_id);
				}

				if (in_shadow == 0)
				{
					color = color_add_color(
						color,
						color_mult_color(
							brdf_f(sr->material->diffuse, sr, &wi, wo),
							color_mult_scalar(
								light_l(sr->world->light_arr[j], sr),
								ndotwi * dist_atenuation
							)
						)
					);
				}				
			}			
		}
	}
	return (color);
}
