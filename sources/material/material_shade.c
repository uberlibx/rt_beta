#include "rt.h"

t_color			material_shade(t_shaderec *sr)
{
	t_color		color;

	color = color_new(255, 0, 0);
	if (sr->material->type == 0)
		color = shade_matte(sr);
	else if (sr->material->type == 1)
		color = shade_phong(sr);
	return (color);
}
