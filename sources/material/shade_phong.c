#include "rt.h"

/*
** Phong material with SHADOWS
*/

t_color			shade_phong(t_shaderec *sr)
{
	t_color		color; // a.k.a. L
	t_dir		wo;
	t_dir		wi;
	float		ndotwi;
	//int			num_lights;
	//int			j;
	t_color		c_temp;
	int			in_shadow;
	t_ray		sray;

	t_llist		*ltemp;

	//t_ray		original_sray_origin = ray->origin;

	wo = dir_mult_scalar(sr->ray->dir, -1.0);
	color = color_mult_color(
		brdf_rho(sr->material->ambient, sr, wo),
		light_l(&sr->world->ambient, sr)
	);

	//num_lights = sr->world->light_arr_len;
	ltemp = sr->world->light_list;
//	j = -1;
	while (ltemp)
	{
		if (ltemp->light.ls > 0)
		{
			wi = light_get_direction(&ltemp->light, sr);
			ndotwi = nl_dot_dir(sr->normal, wi);

			if (ndotwi > 0)
			{
				in_shadow = 0;
				if (ltemp->light.cast_shadow == 1)
				{
					sray = ray_new(sr->hit_point, wi);

					// [TODO]
					//sray.origin = matrix_mult_p3d(sr->world->ob_arr[sr->ob_id]->inverse_transform, sray.origin);
					//sray.origin.y += 85;
					//sray.dir = matrix_mult_dir(sr->world->ob_arr[sr->ob_id]->inverse_transform, sray.dir);
					//dir_normalize(&sray.dir);

					in_shadow = hit_shadow(sr->world, &sray, &ltemp->light, sr->hit_object);
				}

				if (in_shadow == 0)
				{
					color = color_add_color(
						color,
						color_mult_color(
							color_add_color(
								brdf_f(sr->material->diffuse, sr, &wi, wo),
								brdf_f(sr->material->specular, sr, &wi, wo)
							),
							color_mult_scalar(
								light_l(&ltemp->light, sr),
								ndotwi
							)
						)
					);
				}

				/*
				if (in_shadow == 0)
				{
					c_temp = color_add_color(
						brdf_f(sr->material->diffuse, sr, &wi, wo),
						color_mult_color(
							brdf_f(sr->material->specular, sr, &wi, wo)
							color_mult_scalar(
								light_l(sr->world->light_arr[j], sr),
								ndotwi
							)
						)
					);
					color = color_add_color(color, c_temp);
				}
				*/
			}
		}
		ltemp = ltemp->next;
	}

	return (color);
}
