#include "rt.h"

/*
** Matte material with SHADOWS
*/

t_color			shade_matte(t_shaderec *sr)
{
	t_color		color; // a.k.a. L
	t_dir		wo;
	t_dir		wi;
	float		ndotwi;
	int			in_shadow;
	t_ray		sray;
	double		dist_to_light;
	double		dist_atenuation = 1.0;

	t_llist		*ltemp;

	wo = dir_mult_scalar(sr->ray->dir, -1.0);

	color = color_mult_color(
		brdf_rho(sr->material->ambient, sr, wo),
		light_l(&sr->world->ambient, sr)
	);

	ltemp = sr->world->light_list;
	while (ltemp)
	{
		//t_p3d	pt = sr->world->light_arr[j]->location;
		if (ltemp->light.ls > 0)
		{
	//	printf("hfxzh\n");
		//exit(0);
			//sr->hit_point = matrix_mult_p3d(sr->world->ob_arr[sr->ob_id]->transform, sr->hit_point);

			dist_atenuation = 1;
			if (ltemp->light.type == -2)
			{
				dist_to_light = dist_btwn_2_p3d(sr->hit_point, ltemp->light.location);
				dist_atenuation = 1.0 - dist_to_light / 100;
				if (dist_atenuation < 0.0)
					dist_atenuation = 0.0;
			}

			wi = light_get_direction(&ltemp->light, sr);
			//wi = dir_mult_scalar(wi, -1);
			//printf("hit: x: %f  y: %f  z: %f\n", sr->hit_point.x, sr->hit_point.y, sr->hit_point.z);
			//printf("dir: x: %f  y: %f  z: %f\n", ltemp->light.dir.x, ltemp->light.dir.y, ltemp->light.dir.z);
			//printf("wi: x: %f  y: %f  z: %f\n", wi.x, wi.y, wi.z);

			//exit(0);

			t_dir	wi_tr;
			wi_tr = matrix_mult_dir(sr->hit_object->inverse_transform, wi);

			ndotwi = nl_dot_dir(sr->normal, wi);
		//	printf("normal: x: %f  y: %f  z: %f\n", sr->normal.x, sr->normal.y, sr->normal.z);
			//printf("wi: x: %f  y: %f  z: %f\n", wi.x, wi.y, wi.z);
			//ndotwi = nl_dot_dir(sr->normal, wi_tr);
			//printf("%f\n", ndotwi);
			//exit(0);
			if (ndotwi > 0)
			{
				in_shadow = 0;
				if (ltemp->light.cast_shadow == 1)
				{
					t_p3d	hit_tr = sr->hit_point;
					hit_tr = matrix_mult_p3d(sr->hit_object->transform, hit_tr);

					sray = ray_new(sr->hit_point, wi);
					//sray = ray_new(hit_tr, wi);

					//sray.origin = matrix_mult_p3d(sr->world->ob_arr[sr->ob_id]->inverse_transform, sray.origin);
					//sray.dir = matrix_mult_dir(sr->world->ob_arr[sr->ob_id]->inverse_transform, sray.dir);

					in_shadow = hit_shadow(sr->world, &sray, &ltemp->light, sr->hit_object);
				}

				if (in_shadow == 0)
				{
					//printf("gdfh\n");
					//exit(0);
					/*
					t_color x = color_mult_scalar(
						light_l(&ltemp->light, sr),
						ndotwi * dist_atenuation
					);
					build_color(&x);
					printf("%#x\n", x.color);
					*/

					color = color_add_color(
						color,
						color_mult_color(
							brdf_f(sr->material->diffuse, sr, &wi, wo),
							//brdf_f(sr->material->diffuse, sr, &wi_tr, wo),
							color_mult_scalar(
								light_l(&ltemp->light, sr),
								ndotwi * dist_atenuation
							)
						)
					);
				}
			}
		}
		ltemp = ltemp->next;
	}
	return (color);
}
