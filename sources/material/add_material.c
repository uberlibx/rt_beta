#include "rt.h"

static void	add_default(t_world *world, t_material material)
{
	t_mlist		*mtemp;

	mtemp = world->default_materials;
	while (mtemp && mtemp->next)
		mtemp = mtemp->next;
	if (world->default_materials)
	{
		mtemp->next = (t_mlist*)malloc(sizeof(t_mlist));
		mtemp->next->material = material;
		mtemp->next->next = NULL;
	}
	else
	{
		world->default_materials = (t_mlist*)malloc(sizeof(t_mlist));
		world->default_materials->material = material;
		world->default_materials->next = NULL;
	}
}

static void	add_custom(t_world *world, t_material material)
{
	t_mlist		*mtemp;

	mtemp = world->custom_materials;
	while (mtemp && mtemp->next)
		mtemp = mtemp->next;
	if (world->custom_materials)
	{
		mtemp->next = (t_mlist*)malloc(sizeof(t_mlist));
		mtemp->next->material = material;
		mtemp->next->next = NULL;
	}
	else
	{
		world->custom_materials = (t_mlist*)malloc(sizeof(t_mlist));
		world->custom_materials->material = material;
		world->custom_materials->next = NULL;
	}
}

void		add_material(t_world *world, double *params)
{
	t_material	material;

	material = material_new(params);
	if (material.source)
		add_custom(world, material);
	else
		add_default(world, material);
}
