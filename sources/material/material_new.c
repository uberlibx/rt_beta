#include "rt.h"

t_material		material_new(double *params)
{
	t_material	material;
	t_color		color;

	material.source = params[0];
	material.id = (int)params[1];
	material.type = (int)params[2];
	color = color_new(params[3], params[4], params[5]);
	material.ambient = brdf_new_lamb(color, params[6]);
	material.diffuse = brdf_new_lamb(color, params[7]);
	if (material.type == 0)
		material.specular = NULL;
	if (material.type == 1)
	{
		//material.specular = brdf_new_spec(color, 0.25);
		material.specular = brdf_new_glos(color, params[8], params[9]);
	}
	return (material);
}
