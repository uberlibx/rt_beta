#include "rt.h"

t_brdf		*brdf_new_glos(t_color color, double ks, double exp)
{
	t_brdf	*brdf;

	brdf = brdf_new(2);
	brdf->cd = color;
	brdf->cs = color;
	brdf->ks = ks;
	brdf->exp = exp;
	return(brdf);
}
