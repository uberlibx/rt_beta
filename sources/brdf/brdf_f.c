#include "rt.h"

t_color		brdf_f(t_brdf *brdf, t_shaderec *sr, t_dir *wi, t_dir wo)
{
	t_color		color;

	color = color_new(255, 0, 0);
	if (brdf->type == 0)
		color = lamb_f(brdf, sr, wi, wo);
	//else if (brdf->type == 1)
	//	color = spec_f(brdf, sr, wi, wo);
	else if (brdf->type == 2)
		color = glos_f(brdf, sr, wi, wo);
	return (color);
}
