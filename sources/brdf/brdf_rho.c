#include "rt.h"

t_color		brdf_rho(t_brdf *brdf, t_shaderec *sr, t_dir wo)
{
	t_color		color;

	color = color_new(255, 0, 0);
	if (brdf->type == 0)
		color = lamb_rho(brdf, sr, wo);
	//else if (brdf->type == 1)
	//	color = spec_rho(brdf, sr, wo);
	//else if (brdf->type == 2)
	//	color = glos_rho(brdf, sr, wo);
	return (color);
}
