#include "rt.h"

t_color		lamb_f(t_brdf *brdf, t_shaderec *sr, t_dir *wi, t_dir wo)
{
	t_color		color;

	color = color_mult_scalar(brdf->cd, brdf->kd * INV_PI);
	return (color);
}
