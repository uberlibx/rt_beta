#include "rt.h"

t_brdf		*brdf_new(int type)
{
	t_brdf	*brdf;

	brdf = (t_brdf*)malloc(sizeof(t_brdf));
	brdf->type = type;
	brdf->sampler = sampler_new(SETS, SAMPLES, 4); // 4 - mjit
	brdf->normal = nl_new(0, 1, 0);

	brdf->cd = color_new(88, 88, 88);
	brdf->kd = 0.5;

	brdf->cr = color_new(88, 88, 88);
	brdf->kr = 0.5;

	brdf->cs = color_new(88, 88, 88);
	brdf->ks = 0.5;
	brdf->exp = 3.0;
	return(brdf);
}
