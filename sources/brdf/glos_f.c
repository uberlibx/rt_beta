#include "rt.h"

t_color		glos_f(t_brdf *brdf, t_shaderec *sr, t_dir *wi, t_dir wo)
{
	t_color		color;
	float		ndotwi;
	t_nl		r;
	float		rdotwo;

	color = color_new(0, 0, 0);

	ndotwi = nl_dot_dir(sr->normal, *wi);

	r = nl_add_dir(
		nl_mult_scalar(sr->normal, 2 * ndotwi),
		dir_mult_scalar(*wi, -1)
	);

	rdotwo = nl_dot_dir(r, wo);

	if (rdotwo > 0)
		color = color_mult_scalar(brdf->cs, brdf->ks * pow(rdotwo, brdf->exp));
	return (color);
}
