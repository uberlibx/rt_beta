#include "rt.h"

t_brdf		*brdf_new_spec(t_color color, double kd)
{
	t_brdf	*brdf;

	brdf = brdf_new(1);
	brdf->cd = color;
	brdf->cr = color;
	brdf->kr = kd;
	return(brdf);
}
