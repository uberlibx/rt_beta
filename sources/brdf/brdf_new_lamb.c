#include "rt.h"

t_brdf		*brdf_new_lamb(t_color color, double kd)
{
	t_brdf	*brdf;

	brdf = brdf_new(0);
	brdf->cd = color;
	brdf->kd = kd;
	return(brdf);
}
