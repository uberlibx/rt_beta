#include "rt.h"

void	add_object(t_world *world, double *params)
{
	t_ob	object;
	t_olist	*otemp;

	object = ob_new(params);
	otemp = world->obj_list;
	while (otemp && otemp->next)
		otemp = otemp->next;
	if (world->obj_list)
	{
		otemp->next = (t_olist*)malloc(sizeof(t_olist));
		otemp->next->object = object;
		otemp->next->next = NULL;
	}
	else
	{
		world->obj_list = (t_olist*)malloc(sizeof(t_olist));
		world->obj_list->object = object;
		world->obj_list->next = NULL;
	}
}
