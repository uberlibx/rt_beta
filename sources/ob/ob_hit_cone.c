#include "rt.h"

int				ob_hit_cone(t_ob *ob, t_ray *ray, double *tmin, t_shaderec *sr)
{
	t_dir	pt_1;

	double	A = ray->origin.x - ob->center.x;
    double	B = ray->origin.z - ob->center.z;
    double	D = ob->height - ray->origin.y + ob->center.y;

    double	tan = (ob->radius / ob->height) * (ob->radius / ob->height);
    double	a = (ray->dir.x * ray->dir.x) + (ray->dir.z * ray->dir.z) - (tan*(ray->dir.y * ray->dir.y));
    double	b = (2 * A * ray->dir.x) + (2 * B * ray->dir.z) + (2 * tan * D * ray->dir.y);
    double	c = (A * A) + (B * B) - (tan * (D * D));

    double	delta = b * b - 4 * (a * c);
	if(fabs(delta) < 0.001) return 0;
    if(delta < 0.0) return 0;

    double	t1 = (-b - sqrt(delta))/(2*a);
    double	t2 = (-b + sqrt(delta))/(2*a);
    double	t;

    if (t1>t2) t = t2;
    else t = t1;

    double	r = ray->origin.y + t * ray->dir.y;

    if ((r > ob->center.y) && (r < ob->center.y + ob->height))
    {
		// Distance to point of intersection
    	*tmin = t;

		// Point of intersection
		pt_1 = dir_mult_scalar(ray->dir, -t);
		sr->local_hit_point = p3d_add_dir(ray->origin, pt_1);

		// Normal at point of intersection


    	float r = sqrt((sr->local_hit_point.x - ob->center.x) * (sr->local_hit_point.x - ob->center.x) +
						(sr->local_hit_point.z - ob->center.z) * (sr->local_hit_point.z - ob->center.z));
    	sr->normal.x = sr->local_hit_point.x - ob->center.x;
    	sr->normal.y = r * (ob->radius / ob->height);
    	sr->normal.z = sr->local_hit_point.z - ob->center.z;
    	nl_normalize(&sr->normal);

    	return (1);
    }

	return (0);
}
