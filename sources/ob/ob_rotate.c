#include "rt.h"

// [TODO] function matrix_cpy

void		ob_rotate(t_ob *ob, double rx, double ry, double rz)
{
	double	tr1[4][4];
	double	tr2[4][4];

	// >>> TRANSFORM
	// Rotate
	matrix_identity(&tr1);
	matrix_identity(&tr2);
	tr_rotate(&tr1, rx, ry, rz); // build new rotation transform
	matrix_cpy(ob->tr_rotate, &tr2); // copy current rotation transform
	matrix_mult_matrix(tr2, tr1, &ob->tr_rotate); // set mixed rotation transform

	// FINAL
	matrix_mult_matrix(ob->tr_rotate, ob->tr_translate, &ob->transform);
	/*
	matrix_identity(&tr1);
	matrix_identity(&tr2);
	matrix_mult_matrix(tr1, ob->transform, &tr2);
	tr_rotate(&tr1, rx, ry, rz);
	matrix_mult_matrix(tr2, tr1, &ob->transform);
	*/

	// >>> INVERSE TRANSFORM
	// Inverse Rotate
	// Rotate
	matrix_identity(&tr1);
	matrix_identity(&tr2);
	itr_rotate(&tr1, rx, ry, rz); // build new inverse rotation transform
	matrix_cpy(ob->itr_rotate, &tr2); // copy current inverse rotation transform
	matrix_mult_matrix(tr2, tr1, &ob->itr_rotate); // set mixed inverse rotation transform

	// FINAL
	matrix_mult_matrix(ob->itr_rotate, ob->itr_translate, &ob->inverse_transform);
	/*
	matrix_identity(&tr1);
	matrix_identity(&tr2);
	matrix_mult_matrix(tr1, ob->inverse_transform, &tr2);
	transform_rotate(&tr1, rx, ry, rz);
	matrix_mult_matrix(tr2, tr1, &ob->inverse_transform);
	*/
}
