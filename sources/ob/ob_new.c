#include "rt.h"

t_ob			ob_new(double *params)
{
	t_ob	ob;
	double	rot[4][4];

	ob.type = (int)params[0];
	ob.id = (int)params[1];
	ob.center = p3d_new(0, 0, 0);
	ob.radius = params[9];
	ob.height = params[10];
	ob.k_epsilon = 0.0000000001;
	ob.material_id = (int)params[2];
	//ob.material = material_new_matte(color_new(88, 88, 88), 0.25, 0.55); // [TODO]
	ob.cast_shadow = (int)params[11];
	ob.receive_shadow = (int)params[12];
	ob.in_shadow = 0;

	matrix_identity(&ob.transform);
	matrix_identity(&ob.tr_translate);
	matrix_identity(&ob.tr_rotate);
	matrix_identity(&ob.inverse_transform);
	matrix_identity(&ob.itr_translate);
	matrix_identity(&ob.itr_rotate);
	ob_translate(&ob, params[3], params[4], params[5]);
	//ob_rotate(&ob, params[6], params[7], params[8]);

	matrix_identity(&rot);
	tr_rotate(&rot, params[6], params[7], params[8]);

	ob.normal = matrix_mult_nl(rot, nl_new(0, 1, 0));

	return (ob);
}
