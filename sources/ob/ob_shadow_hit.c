#include "rt.h"

int				ob_shadow_hit(t_ob *ob, t_ray *ray, double *tmin)
{
	if (ob->type == 0)
		return (ob_shadow_hit_plane(ob, ray, tmin));
	else if (ob->type == 1)
		return (ob_shadow_hit_sphere(ob, ray, tmin));
	else if (ob->type == 3)
		return (ob_shadow_hit_cylinder(ob, ray, tmin));
	else if (ob->type == 4)
		return (ob_shadow_hit_cone(ob, ray, tmin));
	return (0);
}
