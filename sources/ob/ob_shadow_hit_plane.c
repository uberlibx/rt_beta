#include "rt.h"

int				ob_shadow_hit_plane(t_ob *ob, t_ray *ray, double *tmin)
{
	double	t;
	double	t1;
	double	t2;
	t_dir	dir1;

	dir1 = dir_btwn_2_p3d(ob->center, ray->origin);
	t1 = nl_dot_dir(ob->normal, dir1);
	t2 = nl_dot_dir(ob->normal, ray->dir);
	t = t1 / t2;

	if (t > ob->k_epsilon)
	{
		*tmin = t;
		return (1);
	}
	return (0);
}
