#include "rt.h"

// [TODO] function matrix_cpy

void		ob_translate(t_ob *ob, double dx, double dy, double dz)
{
	double	tr1[4][4];
	double	tr2[4][4];

	// >>> TRANSFORM
	// Translate
	matrix_identity(&tr1);
	tr_translate(&tr1, dx, dy, dz);
	matrix_cpy(ob->tr_translate, &tr2);
	matrix_mult_matrix(tr1, tr2, &ob->tr_translate);

	// FINAL
	matrix_mult_matrix(ob->tr_rotate, ob->tr_translate, &ob->transform);
	/*
	matrix_identity(&tr1);
	matrix_identity(&tr2);
	matrix_mult_matrix(tr1, ob->transform, &tr2);
	tr_translate(&tr1, dx, dy, dz);
	matrix_mult_matrix(tr1, tr2, &ob->transform);
	*/

	// >>> INVERSE TRANSFORM
	// Inverse Translate
	matrix_identity(&tr1);
	itr_translate(&tr1, dx, dy, dz);
	matrix_cpy(ob->itr_translate, &tr2);
	matrix_mult_matrix(tr1, tr2, &ob->itr_translate);

	// FINAL
	matrix_mult_matrix(ob->itr_rotate, ob->itr_translate, &ob->inverse_transform);
	/*
	matrix_identity(&tr1);
	matrix_identity(&tr2);
	matrix_mult_matrix(tr1, ob->inverse_transform, &tr2);
	transform_translate(&tr1, dx, dy, dz);
	matrix_mult_matrix(tr1, tr2, &ob->inverse_transform);
	*/
}
