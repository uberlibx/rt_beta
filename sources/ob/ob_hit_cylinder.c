#include "rt.h"

int				ob_hit_cylinder(t_ob *ob, t_ray *ray, double *tmin, t_shaderec *sr)
{
	double t;
	double ox = ray->origin.x - ob->center.x;
	double oy = ray->origin.y - ob->center.y;
	double oz = ray->origin.z - ob->center.z;
	double dx = ray->dir.x;
	double dy = ray->dir.y;
	double dz = ray->dir.z;

	double a = dx * dx + dz * dz;
	double b = 2.0 * (ox * dx + oz * dz);
	double c = ox * ox + oz * oz - ob->radius * ob->radius;
	double disc = b * b - 4.0 * a * c;

	double y0;
	double y1;

	y0 = 0;
	y1 = ob->height;

	if (disc >= 0)
	{
		double e = sqrt(disc);
		double denom = 2.0 * a;

		t = (-b - e) / denom;    // smaller root
		if (t > ob->k_epsilon)
		{
			double yhit = oy + t * dy;

			if (yhit > y0 && yhit < y1)
			{
				*tmin = t;
				sr->normal = nl_new(
					(ox + t * dx) * 1 / ob->radius,
					0.0,
					(oz + t * dz) * 1 / ob->radius);
				nl_normalize(&sr->normal);

				// test for hitting from inside
				t_dir		inv_ray_dir = dir_mult_scalar(ray->dir, -1.0);
				double ndotd = nl_dot_dir(sr->normal, inv_ray_dir);
				if (ndotd < 0.0)
					sr->normal = nl_mult_scalar(sr->normal, -1);

				sr->local_hit_point = p3d_add_dir(
					ray->origin,
					dir_mult_scalar(ray->dir, *tmin)
				);

				sr->normal = matrix_mult_nl(ob->inverse_transform, sr->normal);
				nl_normalize(&sr->normal);

				return (1);
			}
		}

		t = (-b + e) / denom;    // larger root
		if (t > ob->k_epsilon)
		{
			double yhit = oy + t * dy;

			if (yhit > y0 && yhit < y1)
			{
				*tmin = t;
				sr->normal = nl_new(
					(ox + t * dx) * 1 / ob->radius,
					0.0,
					(oz + t * dz) * 1 / ob->radius);
				nl_normalize(&sr->normal);

				// test for hitting from inside
				t_dir		inv_ray_dir = dir_mult_scalar(ray->dir, -1.0);
				double ndotd = nl_dot_dir(sr->normal, inv_ray_dir);
				if (ndotd < 0.0)
					sr->normal = nl_mult_scalar(sr->normal, -1);

				sr->local_hit_point = p3d_add_dir(
					ray->origin,
					dir_mult_scalar(ray->dir, *tmin)
				);

				sr->normal = matrix_mult_nl(ob->inverse_transform, sr->normal);
				nl_normalize(&sr->normal);

				return (1);
			}
		}
	}
	return (0);
}
