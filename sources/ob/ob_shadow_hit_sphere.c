#include "rt.h"

int				ob_shadow_hit_sphere(t_ob *ob, t_ray *ray, double *tmin)
{
	double	t;
	double	a;
	double	b;
	double	c;
	double	disc;
	double	e;
	double	denom;
	t_dir	temp;

	double	dot1;
	t_dir	dir1;
	t_dir	dir2;
	t_dir	dir3;

	temp = dir_btwn_2_p3d(ray->origin, ob->center);
	a = dir_dot_dir(ray->dir, ray->dir);
	b = 2.0 * dir_dot_dir(temp, ray->dir);
	c = dir_dot_dir(temp, temp) - ob->radius * ob->radius;
	disc = b * b - 4 * a * c;

	if (disc < 0.0)
		return (0);
	else
	{
		e = sqrt(disc);
		denom = 2.0 * a;

		t =(-b - e) / denom;
		if (t > ob->k_epsilon)
		{
			*tmin = t;
			return (1);
		}

		t =(-b + e) / denom;
		if (t > ob->k_epsilon)
		{
			*tmin = t;
			return (1);
		}
	}
	return (0);
}
