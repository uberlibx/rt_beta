#include "rt.h"

int				ob_hit(t_ob *ob, t_ray *ray, double *tmin, t_shaderec *sr)
{
	if (ob->type == 1)
		return (ob_hit_plane(ob, ray, tmin, sr));
	else if (ob->type == 2)// || ob->type == 11)
		return (ob_hit_sphere(ob, ray, tmin, sr));
	else if (ob->type == 3)
		return (ob_hit_disk(ob, ray, tmin, sr));
	else if (ob->type == 4)
		return (ob_hit_cylinder(ob, ray, tmin, sr));
	else if (ob->type == 5)
		return (ob_hit_cone(ob, ray, tmin, sr));
	return (0);
}
