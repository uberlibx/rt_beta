#include "rt.h"

void		data_init(t_data *data)
{
	data->world = world_new();
	data->mlx = mlx_init();
	data->win = mlx_new_window(data->mlx, WIDTH, HEIGHT, TITLE);
	data->img = mlx_new_image(data->mlx, WIDTH, HEIGHT);
	data->bpp = 32;
	data->line = WIDTH;
	data->endian = 0;
	data->pixel = (int*)mlx_get_data_addr(data->img, &data->bpp, &data->line, &data->endian);
	data->is_render = 1;

	data->thread = (t_thread*)malloc(sizeof(t_thread) * THREAD_NR);
	int		i = -1;
	while (++i < THREAD_NR)
	{
		//data->thread[i].id = i;
		data->thread[i].nr = i;
		data->thread[i].start_h = i * HEIGHT / THREAD_NR;
		data->thread[i].end_h = data->thread[i].start_h + HEIGHT / THREAD_NR;
		data->thread[i].data = data;
	}
}
