#include "rt.h"

static void		get_object(double *params, char **tmp)
{
	char	**tmp2;
	int		i;
	int		j;

	//ft_putendl("		get_object: START");
	i = -1;
	j = 0;
	while (++i < 3)
		params[j++] = ft_atoi(tmp[i]);
	while (i < 5)
	{
		tmp2 = ft_strsplit(tmp[i++], ',');
		params[j++] = ft_atod(tmp2[0]);
		params[j++] = ft_atod(tmp2[1]);
		params[j++] = ft_atod(tmp2[2]);
		free(tmp2);
	}
	params[j++] = ft_atod(tmp[i++]);
	params[j++] = ft_atod(tmp[i++]);
	tmp2 = ft_strsplit(tmp[i++], ',');
	params[j++] = ft_atoi(tmp2[0]);
	params[j++] = ft_atoi(tmp2[1]);
	free(tmp2);
	//ft_putendl("		get_object: OK");
}

static void		get_light(double *params, char **tmp)
{
	char	**tmp2;
	int		i;
	int		j;

	//ft_putendl("		get_light: START");
	i = -1;
	j = 0;

	while (++i < 2)
		params[j++] = ft_atoi(tmp[i]);
	while (i < 5)
	{
		tmp2 = ft_strsplit(tmp[i++], ',');
		params[j++] = ft_atod(tmp2[0]);
		params[j++] = ft_atod(tmp2[1]);
		params[j++] = ft_atod(tmp2[2]);
		free(tmp2);
	}
	while (i < 7)
		params[j++] = ft_atod(tmp[i++]);
	//ft_putendl("		get_light: OK");
}

static void		get_camera(double *params, char **tmp)
{
	char	**tmp2;
	int		i;
	int		j;

	//ft_putendl("		get_camera: START");
	i = 0;
	j = 0;

	params[j++] = ft_atod(tmp[i++]);
	while (i < 4)
	{
		tmp2 = ft_strsplit(tmp[i++], ',');
		params[j++] = ft_atod(tmp2[0]);
		params[j++] = ft_atod(tmp2[1]);
		params[j++] = ft_atod(tmp2[2]);
		free(tmp2);
	}
	while (i < 6)
		params[j++] = ft_atoi(tmp[i++]);
	//ft_putendl("		get_camera: OK");
}

static double	*get_params(char *buff)
{
	double	*params;
	char	**tmp;

	tmp = ft_strsplit(buff, '[');
	params = (double*)malloc(sizeof(double) * 13);
	if (atoi(tmp[0]) > 0)
		get_object(params, tmp);
	else if (atoi(tmp[0]) < 0)
		get_light(params, tmp);
	else
		get_camera(params, tmp);
	free(tmp);
	return (params);
}

static void		open_scene(t_data *data, int fd)
{
	char	*buff;
	double	*params;
	int		code;

	while ((code = get_next_line(fd, &buff)) > 0)
	{
		params = get_params(buff);
		free(buff);
		if (params[0] == 0)
		{
		//	printf("\ncamera\n");
		//	printf("id: %i\n", (int)params[1]);
		//	printf("translation: \n x: %f	y: %f	z: %f\n", params[2], params[3], params[4]);
		//	printf("rotation: \n x: %f	y: %f	z: %f\n", params[5], params[6], params[7]);
		//	printf("look_at: \n x: %f	y: %f	z: %f\n", params[8], params[9], params[10]);
		//	printf("focal_distance: %f\n", params[11]);
		//	printf("cast_shadow: %i\n", (int)params[12]);
			set_camera(&data->world, params); // [TODO] make it
		}
		else if (params[0] < 0)
		{
		//	printf("\nlight\n");
		//	printf("id: %i\n", (int)params[1]);
		//	printf("location: \n x: %f	y: %f	z: %f\n", params[2], params[3], params[4]);
		//	printf("rotation: \n x: %f	y: %f	z: %f\n", params[5], params[6], params[7]);
		//	printf("color: \n r: %f	g: %f	b: %f\n", params[8], params[9], params[10]);
		//	printf("intensity: %f\n", params[11]);
		//	printf("cast_shadow: %i\n", (int)params[12]);
			add_light(&data->world, params);
		}
		else if (params[0] > 0)
		{
		//	printf("\nparamsject\n");
		//	printf("id: %i\n", (int)params[1]);
		//	printf("material: %i\n", (int)params[2]);
		//	printf("translation: \n x: %f	y: %f	z: %f\n", params[3], params[4], params[5]);
		//	printf("rotation: \n x: %f	y: %f	z: %f\n", params[6], params[7], params[8]);
		//	printf("radius: %f\n", params[9]);
		//	printf("height: %f\n", params[10]);
		//	printf("shadows: \n cast: %i	receive: %i\n", (int)params[11], (int)params[12]);
			add_object(&data->world, params);
		}
		free(params);
	}
	if (code == -1)
		error(1);
}

void		load_scene(t_data *data, char *file)
{
	int	fd;

	data->name = file;
	if (!ft_strchr(file, '/'))
		file = ft_strjoin("./scenes/", ft_strjoin(file, ".sc"));
	//ft_putstr("	file: ");
	//ft_putendl(file);
	//ft_putendl("	open_scene: START");
	if ((fd = open(file, O_RDONLY)) > 0)
		open_scene(data, fd);
	//ft_putendl("	open_scene: OK");
	//ft_putendl("	load_materials: START");
	load_materials(data);
	//ft_putendl("	load_materials: OK");
	close(fd);
}
