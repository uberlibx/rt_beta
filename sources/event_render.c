#include "rt.h"

int				event_render(t_data *data)
{
	//ft_putendl("event_render: START");
	if (data->is_render == 1)
	{
		int		j;

		//ft_putendl("	threads: START");

		j = -1;
		while (++j < THREAD_NR)
		{
			pthread_create(
					&data->thread[j].id,
					NULL,
					render_scene_pinhole_camera_multithread,
					&data->thread[j]);
		}
		while (--j >= 0)
			pthread_join(data->thread[j].id, NULL);

		//ft_putendl("	threads: OK");

		mlx_put_image_to_window(data->mlx, data->win, data->img, 0, 0);

		//ft_putendl("	image_to_window: OK");

		data->is_render = 0;
	}
	//ft_putendl("event_render: OK");
	return (0);
}
