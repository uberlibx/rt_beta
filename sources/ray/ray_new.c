#include "rt.h"

t_ray		ray_new(t_p3d origin, t_dir dir)
{
	t_ray	ray;

	ray.origin = origin;
	ray.dir = dir;
	return (ray);
}