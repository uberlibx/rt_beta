#include "rt.h"

void			matrix_mult_matrix(double m1[4][4], double m2[4][4], double (*r)[4][4])
{
	int		i;
	int		j;
	int		k;


	i = -1;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
		{
			(*r)[j][i] = 0;
			k = -1;
			while (++k < 4)
				(*r)[j][i] += m1[j][k] * m2[k][i];
		}
	}
}
