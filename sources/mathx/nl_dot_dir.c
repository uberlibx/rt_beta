#include "rt.h"

double			nl_dot_dir(t_nl a, t_dir b)
{
	return (a.x * b.x + a.y * b.y + a.z * b.z);
}