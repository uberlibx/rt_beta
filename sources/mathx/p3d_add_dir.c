#include "rt.h"

t_p3d			p3d_add_dir(t_p3d a, t_dir b)
{
	t_p3d	p;

	p.x = a.x - b.x;
	p.y = a.y - b.y;
	p.z = a.z - b.z;
	return (p);
}