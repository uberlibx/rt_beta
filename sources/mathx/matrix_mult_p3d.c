#include "rt.h"

t_p3d		matrix_mult_p3d(double m[4][4], t_p3d p)
{
	t_p3d	r;

	r.x = m[0][0] * p.x + m[0][1] * p.y + m[0][2] * p.z + m[0][3];
	r.y = m[1][0] * p.x + m[1][1] * p.y + m[1][2] * p.z + m[1][3];
	r.z = m[2][0] * p.x + m[2][1] * p.y + m[2][2] * p.z + m[2][3];
	return (r);
}
