#include "rt.h"

double			dist_btwn_2_p3d(t_p3d a, t_p3d b)
{
	double		d;

	d = sqrt(
		(b.x - a.x) * (b.x - a.x) +
		(b.y - a.y) * (b.y - a.y) +
		(b.z - a.z) * (b.z - a.z)
	);
	return (d);
}
