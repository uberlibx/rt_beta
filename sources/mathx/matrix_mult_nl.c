#include "rt.h"

t_nl		matrix_mult_nl(double m[4][4], t_nl n)
{
	t_nl	r;

	r.x = m[0][0] * n.x + m[1][0] * n.y + m[2][0] * n.z;
	r.y = m[0][1] * n.x + m[1][1] * n.y + m[2][1] * n.z;
	r.z = m[0][2] * n.x + m[1][2] * n.y + m[2][2] * n.z;

	return (r);
}
