#include "rt.h"

double			dir_dot_dir(t_dir a, t_dir b)
{
	return (a.x * b.x + a.y * b.y + a.z * b.z);
}