#include "rt.h"

double			dir_magnitude(t_dir a)
{
	return (sqrt(a.x * a.x + a.y * a.y + a.z * a.z));
}