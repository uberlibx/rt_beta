#include "rt.h"

t_dir			dir_mult_scalar(t_dir a, double d)
{
	t_dir	v;

	v.x = a.x * d;
	v.y = a.y * d;
	v.z = a.z * d;
	return (v);
}