#include "rt.h"

int				rand_int_range(int min, int max)
{
	int		limit;

	limit = max - min;
	if (limit == 0)
		limit = 1;

    return (min + rand() % limit);
}