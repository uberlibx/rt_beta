#include "rt.h"

// INVERSE translation transformation
// m  is assumed to be an identity 4x4 matrix
// [TODD] rename to transform_translate

void		tr_translate(double (*m)[4][4], double tx, double ty, double tz)
{
	(*m)[0][3] = tx;
	(*m)[1][3] = ty;
	(*m)[2][3] = tz;
}
