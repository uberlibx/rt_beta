#include "rt.h"

t_nl			nl_mult_scalar(t_nl a, double d)
{
	t_nl	n;

	n.x = a.x * d;
	n.y = a.y * d;
	n.z = a.z * d;
	return (n);
}