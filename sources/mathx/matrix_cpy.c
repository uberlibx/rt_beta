#include "rt.h"

void			matrix_cpy(double src[4][4], double (*dest)[4][4])
{
	int		i;
	int		j;

	i = -1;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
		{
			(*dest)[j][i] = src[j][i];
		}
	}
}
