#include "rt.h"

void		nl_normalize(t_nl *a)
{
	double	magnitude;

	magnitude = nl_magnitude(*a);
    a->x /= magnitude;
    a->y /= magnitude;
    a->z /= magnitude;
}