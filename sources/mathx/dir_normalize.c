#include "rt.h"

void			dir_normalize(t_dir *v3)
{
	double	magnitude;

	magnitude = dir_magnitude(*v3);
    v3->x /= magnitude;
    v3->y /= magnitude;
    v3->z /= magnitude;
}