#include "rt.h"

double			**matrix_new()
{
	double		**m;
	int			i;

	m = (double**)malloc(sizeof(double*) * 4);
	i = -1;
	while (++i < 4)
		m[i] = (double*)malloc(sizeof(double) * 4);
	return (m);
}
