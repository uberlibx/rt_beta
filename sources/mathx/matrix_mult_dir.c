#include "rt.h"

t_dir		matrix_mult_dir(double m[4][4], t_dir dir)
{
	t_dir	r;

	r.x = m[0][0] * dir.x + m[0][1] * dir.y + m[0][2] * dir.z;
	r.y = m[1][0] * dir.x + m[1][1] * dir.y + m[1][2] * dir.z;
	r.z = m[2][0] * dir.x + m[2][1] * dir.y + m[2][2] * dir.z;

	return (r);
}
