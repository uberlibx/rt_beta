#include "rt.h"

double			rand_dbl_range(double min, double max)
{
	double	range;
	double	div;

	range = (max - min);
	div = RAND_MAX / range;
    return (min + (rand() / div)) / 4.0;
}