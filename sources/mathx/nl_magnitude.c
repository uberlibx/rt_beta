#include "rt.h"

double			nl_magnitude(t_nl a)
{
	return (sqrt(a.x * a.x + a.y * a.y + a.z * a.z));
}