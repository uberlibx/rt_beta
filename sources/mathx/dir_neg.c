#include "rt.h"

t_dir			dir_neg(t_dir a)
{
	t_dir		dir;

	dir.x = -a.x;
	dir.y = -a.y;
	dir.z = -a.z;

	return (dir);
}