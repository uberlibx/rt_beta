#include "rt.h"

void			matrix_identity(double (*m)[4][4])
{
	int		i;
	int		j;

	i = -1;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
		{
			if (i == j)
				(*m)[i][j] = 1;
			else
				(*m)[i][j] = 0;
		}
	}
}
