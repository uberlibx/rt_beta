#include "rt.h"

t_nl			nl_from_p3d(t_p3d a)
{
	t_nl	n;

	n.x = a.x;
	n.y = a.y;
	n.z = a.z;
	return (n);
}
