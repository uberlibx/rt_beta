#include "rt.h"

// TRANSFORM rotation
// m  is assumed to be an identity 4x4 matrix
// the angles are in degrees

static void		get_rot_x(double (*m)[4][4], double r)
{
	(*m)[1][1] = cos(r);
	(*m)[2][1] = -sin(r);
	(*m)[1][2] = sin(r);
	(*m)[2][2] = cos(r);
}

static void		get_rot_y(double (*m)[4][4], double r)
{
	(*m)[0][0] = cos(r);
	(*m)[2][0] = sin(r);
	(*m)[0][2] = -sin(r);
	(*m)[2][2] = cos(r);
}

static void		get_rot_z(double (*m)[4][4], double r)
{
	(*m)[0][0] = cos(r);
	(*m)[1][0] = -sin(r);
	(*m)[0][1] = sin(r);
	(*m)[1][1] = cos(r);
}

void			tr_rotate(double (*m)[4][4], double rx, double ry, double rz)
{
	double	rot_x[4][4];
	double	rot_y[4][4];
	double	rot_z[4][4];
	double	rot_temp[4][4];
	double	rot_temp2[4][4];

	rx *= TO_RAD;
	ry *= TO_RAD;
	rz *= TO_RAD;


	matrix_identity(&rot_x);
	matrix_identity(&rot_y);
	matrix_identity(&rot_z);

	get_rot_x(&rot_x, rx);
	get_rot_y(&rot_y, ry);
	get_rot_z(&rot_z, rz);

	matrix_mult_matrix(rot_x, *m, &rot_temp);
	matrix_mult_matrix(rot_y, rot_temp, &rot_temp2);
	matrix_mult_matrix(rot_z, rot_temp2, m);

	get_rot_y(m, ry);
}
