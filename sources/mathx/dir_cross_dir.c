#include "rt.h"

t_dir			dir_cross_dir(t_dir a, t_dir b)
{
	t_dir	v;

	v.x = a.y * b.z - a.z * b.y;
	v.y = a.z * b.x - a.x * b.z;
	v.z = a.x * b.y - a.y * b.x;
	return (v);
}