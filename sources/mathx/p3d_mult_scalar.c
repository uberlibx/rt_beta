#include "rt.h"

t_p3d			p3d_mult_scalar(t_p3d a, double n)
{
	t_p3d	p;

	p.x = a.x * n;
	p.y = a.y * n;
	p.z = a.z * n;
	return (p);
}