#include "rt.h"

t_dir			dir_btwn_2_p3d(t_p3d a, t_p3d b)
{
	t_dir	v;

	v.x = a.x - b.x;
	v.y = a.y - b.y;
	v.z = a.z - b.z;
	return (v);
}