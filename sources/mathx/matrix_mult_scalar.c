#include "rt.h"

void			matrix_mult_scalar(double (*m)[4][4], double d)
{
	int		i;
	int		j;

	i = -1;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
		{
			(*m)[i][j] *= d;
		}
	}
}
