#include "rt.h"

t_dir			dir_sub_dir(t_dir a, t_dir b)
{
	t_dir	v;

	v.x = a.x - b.x;
	v.y = a.y - b.y;
	v.z = a.z - b.z;
	return (v);
}