#include "rt.h"

// [TODEL] - for testing only, using printf
void			matrix_print(double m[4][4])
{
	int		i;
	int		j;

	i = -1;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
		{
			printf ("%f ", m[i][j]);
		}
		printf ("\n");
	}
}
