#include "rt.h"

t_camera			camera_new(t_p3d eye, t_p3d look_at, double d)
{
	t_camera	camera;

	camera.eye = eye;
	camera.look_at = look_at;
	camera.d = d;
	camera.up = dir_new(0, 1, 0);
	camera.zoom = 1;
	camera.exposure_time = 1;
	return (camera);
}
