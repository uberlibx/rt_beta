#include "rt.h"

t_dir			ray_direction(t_camera *camera, t_p2d p)
{
	t_dir	dir;
	t_dir	dx;
	t_dir	dy;
	t_dir	dz;
	t_dir	t;

	dx = dir_mult_scalar(camera->u, p.x);
	dy = dir_mult_scalar(camera->v, p.y);
	dz = dir_mult_scalar(camera->w, -camera->d);

	t = dir_add_dir(dx, dy);
	dir = dir_add_dir(t, dz);

	dir_normalize(&dir);
	return (dir);
}