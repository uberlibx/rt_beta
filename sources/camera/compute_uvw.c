#include "rt.h"

void			compute_uvw(t_camera *camera)
{
	camera->w = dir_btwn_2_p3d(camera->eye, camera->look_at);
	dir_normalize(&camera->w);

	camera->u = dir_cross_dir(camera->up, camera->w);
	dir_normalize(&camera->u);

	camera->v = dir_cross_dir(camera->w, camera->u);
	//dir_normalize(&camera->v);

	// take care of the singularity by hardwiring in specific camera orientations
	if (camera->eye.x == camera->look_at.x && camera->eye.z == camera->look_at.z && camera->eye.y > camera->look_at.y) { // camera looking vertically down
		camera->u = dir_new(0, 0, 0); camera->u.z = 1;
		camera->v = dir_new(0, 0, 0); camera->v.x = 1;
		camera->w = dir_new(0, 0, 0); camera->w.y = 1;
	}
	
	if (camera->eye.x == camera->look_at.x && camera->eye.z == camera->look_at.z && camera->eye.y < camera->look_at.y) { // camera looking vertically up
		camera->u = dir_new(0, 0, 0); camera->u.x = 1;
		camera->v = dir_new(0, 0, 0); camera->v.z = 1;
		camera->w = dir_new(0, 0, 0); camera->w.y = -1;
	}
}