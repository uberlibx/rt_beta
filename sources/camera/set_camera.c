#include "rt.h"

void	set_camera(t_world *world, double *params)
{
	world->camera.eye = p3d_new(params[1], params[2], params[3]);
	world->camera.look_at = p3d_new(params[7], params[8], params[9]);
	world->camera.d = params[10];
	world->camera.up = dir_new(0, 1, 0);
	world->camera.zoom = 1;
	world->camera.exposure_time = 1;
}
