#include "rt.h"

void			build_color(t_color *a)
{
	unsigned int	r = a->r;
	unsigned int	g = a->g;
	unsigned int	b = a->b;

	if (r > 255)
		r = 255;
	if (g > 255)
		g = 255;
	if (b > 255)
		b = 255;
	a->color = (1 << 24) + (r << 16) + (g << 8) + (b | 0);
}
