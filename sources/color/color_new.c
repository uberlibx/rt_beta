#include "rt.h"

t_color			color_new(float r, float g, float b)
{
	t_color		c;

	c.r = r;
	c.g = g;
	c.b = b;
	build_color(&c);
	return (c);
}