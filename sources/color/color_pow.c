#include "rt.h"

t_color			color_pow(t_color a, double b)
{
	t_color		c;

	c.r = pow(a.r, b);
	c.g = pow(a.g, b);
	c.b = pow(a.b, b);
	return (c);
}