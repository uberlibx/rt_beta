#include "rt.h"

t_color			color_rand()
{
	t_color		c;

	c.r = 255 * rand_dbl_range(0, 50);
	c.g = 255 * rand_dbl_range(0, 50);
	c.b = 255 * rand_dbl_range(80, 120);
	return (c);
}
