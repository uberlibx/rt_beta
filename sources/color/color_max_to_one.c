#include "rt.h"

t_color			color_max_to_one(t_color a)
{
	t_color		c;
	double	t;
	double	total;

	c = a;
	t = a.r;
	if (t < a.g)
		t = a.g;
	if (t < a.b)
		t = a.b;
	if (t > 255.0)
	{
		t = t / 255;
		c.r = a.r / t;
		c.g = a.g / t;
		c.b = a.b / t;
	}
	return (c);
}
