#include "rt.h"

t_color			color_clamp_to_color(t_color a)
{
	t_color		c;

	c = a;
	if (c.r > 255 || c.g > 255 || c.b > 255)
	{
		c.r = 255;
		c.g = 0;
		c.b = 0;
	}
	return (c);
}