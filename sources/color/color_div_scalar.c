#include "rt.h"

t_color			color_div_scalar(t_color a, double b)
{
	t_color		c;

	c.r = a.r / b;
	c.g = a.g / b;
	c.b = a.b / b;
	return (c);
}