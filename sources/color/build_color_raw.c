#include "rt.h"

void			build_color_raw(t_color *a)
{
	unsigned int	r = a->r;
	unsigned int	g = a->g;
	unsigned int	b = a->b;

	a->color = (1 << 24) + (r << 16) + (g << 8) + (b | 0);
}
