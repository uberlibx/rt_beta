#include "rt.h"

t_color			color_mult_color(t_color a, t_color b)
{
	t_color		c;

	c.r = a.r * b.r;
	c.g = a.g * b.g;
	c.b = a.b * b.b;
	return (c);
}