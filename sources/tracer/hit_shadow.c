#include "rt.h"

/*
** Trace ray from point to light, checking all the objects for intersection
*/

int				hit_shadow(t_world *world, t_ray *ray, t_light *light, t_ob *hit)
{
	t_shaderec	*sr;
	int			in_shadow;
	double		t;
	double		to_light_d; // distance to light

	t_olist		*otemp;

	// Transform SRAY
	t_ray	original_ray;
	original_ray = *ray;

	if (light->type == -2)
		to_light_d = dist_btwn_2_p3d(ray->origin, light->location);
	else if (light->type == -1)
		to_light_d = HUGE_VALUE;

	t = 0;
	in_shadow = 0;

	otemp = world->obj_list;
	while (in_shadow == 0 && otemp)
	{
		if (hit->id != otemp->object.id)
		{
			// Transform RAY - to hit objects that have been transformed
			//ray->origin = matrix_mult_p3d(world->ob_arr[ob_id]->transform, ray->origin);
			ray->origin = matrix_mult_p3d(otemp->object.inverse_transform, ray->origin);

			//ray->dir = matrix_mult_dir(world->ob_arr[ob_id]->transform, ray->dir);
			ray->dir = matrix_mult_dir(otemp->object.inverse_transform, ray->dir);
			dir_normalize(&ray->dir);

			if (otemp->object.cast_shadow == 1 && ob_shadow_hit(&otemp->object, ray, &t) && t < to_light_d)
			{
				if ((t > 0 && to_light_d) || (t < 0 && to_light_d < 0))
					in_shadow = 1;
			}

			// Reset SRAY
			*ray = original_ray;
		}
		otemp = otemp->next;

	}

	return (in_shadow);
}
