#include "rt.h"

/*
 * Check if current ray intersects the objects
 * Return the intersection that is closest to the camera
 */

t_shaderec		hit_objects(t_world *world, t_ray *ray)
{
	t_color		color;

	t_shaderec	sr;
	double		tmin;
	double		t;
	int			is_hit;
	int			i;
	t_p3d		local_hit_point;
	t_nl		normal;
	int			ob_id;
	t_ray		original_ray;
	t_ray		last_hit_ray;

	t_olist		*otemp;

	original_ray = *ray;

	sr = shaderec_new(world);
	tmin = HUGE_VALUE;
	i = -1;
	otemp = world->obj_list;
	while (otemp)
	{
		// Transform RAY
		ray->origin = matrix_mult_p3d(otemp->object.inverse_transform, ray->origin);
		ray->dir = matrix_mult_dir(otemp->object.inverse_transform, ray->dir);
		dir_normalize(&ray->dir);

		if (ob_hit(&otemp->object, ray, &t, &sr) && t < tmin)
		{
			last_hit_ray = *ray;

			//ob_id = otemp->object.id;
			sr.hit_object = &otemp->object;
			tmin = t;
			sr.is_hit = 1;
			sr.material = &otemp->object.material;

			sr.hit_point = p3d_add_dir(ray->origin, dir_mult_scalar(ray->dir, -tmin));

			local_hit_point = sr.local_hit_point; // same as hit_point unless object is transformed
			normal = sr.normal;

			sr.color = otemp->object.color; // chapter 3 && no-mats render
		}
		// RESET RAY
		*ray = original_ray;

		otemp = otemp->next;
	}

	// [???] Here or in while
	if (sr.is_hit)
	{
		//sr.ob_id = ob_id;
		sr.t = tmin;
		sr.normal = normal;
		sr.local_hit_point = local_hit_point;
		//*ray = last_hit_ray;
	}
	return (sr);
}
