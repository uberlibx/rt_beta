#include "rt.h"

/*
** Using hit_objects
** Check if current ray hits an object
** If is_hit, call material->shade for that object
*/

t_color			trace_ray(t_world *world, t_ray *ray)
{
	t_shaderec	sr;
	t_color		color;

	//ft_putendl("				hit_objects: START");

	sr = hit_objects(world, ray);

	//ft_putendl("				hit_objects: OK");

	if (sr.is_hit)
	{
	//	printf("hit: id: %i\n", sr.hit_object->id);

		sr.ray = ray; // for specular reflection

		// TRANSFORM hit_point to take into account own transform
	//	sr->hit_point = matrix_mult_p3d(sr->world->ob_arr[sr->ob_id]->transform, sr->hit_point);
		sr.hit_point = matrix_mult_p3d(sr.hit_object->transform, sr.hit_point);
		color = material_shade(&sr); // [FIX]
		//color = color_new(15, 72, 84);
		//color = sr.hit_object->material.diffuse->cd;
	}
	else
		color = world->background_color;
		//color = color_rand();
//	free(sr);
	return (color);
}
