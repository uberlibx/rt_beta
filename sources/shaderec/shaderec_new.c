#include "rt.h"

t_shaderec		shaderec_new(t_world *world)
{
	t_shaderec	sr;

	sr.hit_object = NULL;
	sr.is_hit = 0;
	sr.hit_point = p3d_new(0, 0, 0);
	sr.local_hit_point = p3d_new(0, 0, 0);
	sr.normal = nl_new(0, 0, 0);
	sr.material = NULL;
	sr.ray = NULL;

	sr.world = world;

	sr.color = color_new(0, 0, 0);

	sr.depth = 0;
	sr.dir = dir_new(0, 0, 0);
	return (sr);
}
