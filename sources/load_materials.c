#include "rt.h"

static double *get_params(char *buff)
{
	double	*params;
	char	**tmp;
	char	**tmp2;
	int		i;
	int		j;

	tmp = ft_strsplit(buff, '[');
	params = (double*)malloc(sizeof(double) * 10);
	i = 0;
	j = 1;
	while (i < 2)
		params[j++] = ft_atod(tmp[i++]);
	tmp2 = ft_strsplit(tmp[i++], ',');
	params[j++] = ft_atod(tmp2[0]);
	params[j++] = ft_atod(tmp2[1]);
	params[j++] = ft_atod(tmp2[2]);
	free (tmp2);
	while (i < 7)
		params[j++] = ft_atod(tmp[i++]);
	return (params);
}

static void	load_defaults(t_data *data)
{
	int		fd;
	char	*buff;
	double	*params;

	if ((fd = open("./materials/defaults.mat", O_RDONLY)))
	{
		while(get_next_line(fd, &buff) > 0)
		{
			params = get_params(buff);
			free(buff);
			params[0] = 0;
			add_material(&data->world, params);
			free(params);
		}
	}
	close(fd);
}

static void	load_customs(t_data *data)
{
	int		fd;
	char	*buff;
	double	*params;

	if ((fd = open(ft_strjoin("./materials/", ft_strjoin(data->name, ".mat")), O_RDONLY)))
	{
		while(get_next_line(fd, &buff) > 0)
		{
			params = get_params(buff);
			free(buff);
			params[0] = 1;
			add_material(&data->world, params);
			free(params);
		}
	}
	close(fd);
}

static void	assign_materials(t_olist *obj, t_mlist *defaults, t_mlist *customs)
{
	t_olist	*otemp;
	t_mlist	*mtemp;

	otemp = obj;
	while (otemp)
	{
		if (otemp->object.material_id < 0)
			mtemp = defaults;
		else
			mtemp = customs;
		while (mtemp)
		{
			if (otemp->object.material_id == mtemp->material.id)
			{
				otemp->object.material = mtemp->material;
				break ;
			}
			mtemp = mtemp->next;
		}
		otemp = otemp->next;
	}
}

void		load_materials(t_data *data)
{
	load_defaults(data);
	//ft_putendl("		load_defaults: OK");
	load_customs(data);
	//ft_putendl("		load_customs: OK");
	assign_materials(data->world.obj_list, data->world.default_materials,
		data->world.custom_materials);
	//ft_putendl("		assign_materials: OK");
}
