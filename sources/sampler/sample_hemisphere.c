#include "rt.h"

t_p3d			sample_hemisphere(t_sampler *sampler)
{
	int		index;
	
	if (sampler->count % sampler->num_samples == 0)
		sampler->jump = (rand_int() % sampler->num_sets) * sampler->num_samples;
	index = sampler->jump + sampler->shuffled_indices[sampler->jump + sampler->count++ % sampler->num_samples];
	return (sampler->hemisphere_samples[index]);
}