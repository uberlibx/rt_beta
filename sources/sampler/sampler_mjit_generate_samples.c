#include "rt.h"

static void		shuffle_x_coordinates_special(t_sampler *sampler, int n)
{
	int		p;
	int		i;
	int		j;
	int		k;
	double	t;

	p = -1;
	while (++p < sampler->num_sets)
	{
		i = -1;
		while (++i < n)
		{
			j = -1;
			while (++j < n)
			{
				k = rand_int_range(j, n - 1);
				t = sampler->samples[i * n + j + p * sampler->num_samples].x;
				sampler->samples[i * n + j + p * sampler->num_samples].x = sampler->samples[i * n + k + p * sampler->num_samples].x;
				sampler->samples[i * n + k + p * sampler->num_samples].x = t;
			}
		}
	}
}

static void		shuffle_y_coordinates_special(t_sampler *sampler, int n)
{
	int		p;
	int		i;
	int		j;
	int		k;
	double	t;

	p = -1;
	while (++p < sampler->num_sets)
	{
		i = -1;
		while (++i < n)
		{
			j = -1;
			while (++j < n)
			{
				k = rand_int_range(j, n - 1);
				t = sampler->samples[i * n + j + p * sampler->num_samples].x;
				sampler->samples[i * n + j + p * sampler->num_samples].y = sampler->samples[i * n + k + p * sampler->num_samples].y;
				sampler->samples[i * n + k + p * sampler->num_samples].y = t;
			}
		}
	}
}

void			sampler_mjit_generate_samples(t_sampler *sampler)
{
	int		n;
	int		p;
	int		i;
	int		j;
	float	subcell_width;

	n = sqrt(sampler->num_samples);
	subcell_width = 1.0 / sampler->num_samples;

	// distribute points in the initial patterns
	p = -1;
	while (++p < sampler->num_sets)
	{
		i = -1;
		while (++i < n)
		{
			j = -1;
			while (++j < n)
			{
				sampler->samples[i * n + j + p * sampler->num_samples].x = (i * n + j) * subcell_width + rand_dbl_range(0, subcell_width);
				sampler->samples[i * n + j + p * sampler->num_samples].y = (i * n + j) * subcell_width + rand_dbl_range(0, subcell_width);
			}
		}
	}
	shuffle_x_coordinates_special(sampler, n);
	shuffle_y_coordinates_special(sampler, n);
}