#include "rt.h"

void			random_shuffle(int *arr, int first, int last)
{
	int		len;
	int		i;
	int		t_index;
	int		t_value;

	len = last - first;
	i = -1;
	while (++i < len)
	{
		t_index = rand_int() % (i + 1);
		t_value = arr[i];
		arr[i] = arr[t_index];
		arr[t_index] = t_value;
	}
}