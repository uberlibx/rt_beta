#include "rt.h"

void			map_samples_to_unit_disk(t_sampler *sampler)
{
	int			size; // size of disk_samples array
	double		r; // polar coordinate
	double		phi; // polar coordinate
	t_p2d		sp; // sample point on a unit disk
	int			j;

	size = sampler->num_sets * sampler->num_samples;
	sampler->disk_samples = (t_p2d*)malloc(sizeof(t_p2d) * size);
	j = -1;
	while (++j < size)
	{
		// map sample point to [-1, 1] x [-1, 1]
		sp.x = 2.0 * sampler->samples[j].x - 1.0;
		sp.y = 2.0 * sampler->samples[j].y - 1.0;

		// Calculate r and phi based on the sector the sample point is
		if (sp.x > -sp.y) // sectors 1 and 2
		{
			if (sp.x > sp.y) // sector 1
			{
				r = sp.x;
				phi = sp.y / sp.x;
			}
			else // sector 2
			{
				r = sp.y;
				phi = 2 - sp.x / sp.y;
			}
		}
		else // sectors 3 and 4
		{
			if (sp.x < sp.y) // sector 3
			{
				r = -sp.x;
				phi = 4 + sp.y / sp.x;
			}
			else // sector 4
			{
				r = -sp.y;
				if (sp.y != 0.0) // avoid division by zero at origin
					phi = 6 - sp.x / sp.y;
				else
					phi  = 0.0;
			}
		}

		// convert to radians
		phi *= M_PI / 4.0;

		sampler->disk_samples[j].x = r * cos(phi);
		sampler->disk_samples[j].y = r * sin(phi);
	}
	//free(sampler->samples); // ??? destroy samples on unit square ???
}