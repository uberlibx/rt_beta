#include "rt.h"

void			sampler_nrk_generate_samples(t_sampler *sampler)
{
	int		index;
	int		p;
	int		j;

	index = -1;
	p = -1;
	while (++p < sampler->num_sets)
	{
		j = -1;
		while (++j < sampler->num_samples)
		{
			sampler->samples[++index].x = (j + rand_dbl()) / sampler->num_samples;
			sampler->samples[index].y = (j + rand_dbl()) / sampler->num_samples;
		}
		shuffle_x_coordinates(sampler);
		shuffle_y_coordinates(sampler);
	}
}