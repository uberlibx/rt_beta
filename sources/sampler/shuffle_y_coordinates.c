#include "rt.h"

void			shuffle_y_coordinates(t_sampler *sampler)
{
	int		p;
	int		i;
	int		target;
	double	temp;

	p = -1;
	while (++p < sampler->num_sets)
	{
		i = -1;
		while (++i < sampler->num_samples)
		{
			target = rand_int() % sampler->num_samples + p * sampler->num_samples;
			temp = sampler->samples[i + p * sampler->num_samples + 1].y;
			sampler->samples[i + p * sampler->num_samples + 1].y = sampler->samples[target].y;
			sampler->samples[target].y = temp;
		}
	}
}