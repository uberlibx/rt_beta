#include "rt.h"

void			shuffle_samples(t_sampler *sampler, int type)
{
	if (type == 0) // Regular sampling
		sampler_reg_generate_samples(sampler);

	else if (type == 1) // Pure random
		sampler_pr_generate_samples(sampler);

	else if (type == 2) // Jittered
		sampler_jit_generate_samples(sampler);

	else if (type == 3) // NRK
		sampler_nrk_generate_samples(sampler);

	else if (type == 4) // MJIT
		sampler_mjit_generate_samples(sampler);

	else if (type == 5) // HAM
		sampler_ham_generate_samples(sampler);
}
