#include "rt.h"

static double	get_phi(int j)
{
	double x;
	double f; 
	
	x = 0.0;
	f = 0.5;
	while (j)
	{
		x += f * (double) (j % 2);
		j /= 2;
		f *= 0.5; 
	}
	
	return (x);
}

void			sampler_ham_generate_samples(t_sampler *sampler)
{
	int		index;
	int		p;
	int		j;

	index = -1;
	p = -1;
	while (++p < sampler->num_sets)
	{
		j = -1;
		while (++j < sampler->num_samples)
		{
			sampler->samples[++index].x = (float)j / (float)sampler->num_samples;
			sampler->samples[index].y = get_phi(j);
		}
	}
}