#include "rt.h"

/*
 * num_samples needs to be a perfect square !
 */

void			sampler_jit_generate_samples(t_sampler *sampler)
{
	int		index;
	int		n;
	int		p;
	int		j;
	int		k;

	n = sqrt(sampler->num_samples);
	index = -1;
	p = -1;
	while (++p < sampler->num_sets)
	{
		j = -1;
		while (++j < n)
		{
			k = -1;
			while (++k < n)
			{
				sampler->samples[++index].x = (k + rand_dbl()) / n;
				sampler->samples[index].y = (j + rand_dbl()) / n;
			}
		}
	}
}