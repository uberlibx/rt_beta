#include "rt.h"

// !!! WARNING !!! for hemisphere_samples z is UP !!!
void			map_samples_to_hemisphere(t_sampler *sampler, double exp)
{
	int		size;
	int		j;

	float cos_phi;
	float sin_phi;
	float cos_theta;
	float sin_theta;

	size = sampler->num_sets * sampler->num_samples;
	sampler->hemisphere_samples = (t_p3d*)malloc(sizeof(t_p3d) * size);
	j = -1;
	while (++j < size)
	{
		cos_phi = cos(2.0 * M_PI * sampler->samples[j].x);
		sin_phi = sin(2.0 * M_PI * sampler->samples[j].x);
		cos_theta = pow((1.0 - sampler->samples[j].y), 1.0 / (exp + 1.0));
		sin_theta = sqrt (1.0 - cos_theta * cos_theta);
		sampler->hemisphere_samples[j].x = sin_theta * cos_phi;
		sampler->hemisphere_samples[j].y = sin_theta * sin_phi;
		sampler->hemisphere_samples[j].z = cos_theta;
	}
}