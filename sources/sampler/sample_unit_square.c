#include "rt.h"

t_p2d			sample_unit_square(t_sampler *sampler)
{
	if (sampler->count % sampler->num_samples == 0) // it's a new pixel
		sampler->jump = (rand_int() % sampler->num_sets) * sampler->num_samples;
	int		tmp2 = sampler->jump + sampler->shuffled_indices[sampler->jump + sampler->count++ % sampler->num_samples];
	if (sampler->count > sampler->num_samples)
		sampler->count = 0;
	return (sampler->samples[tmp2]);
}
