#include "rt.h"

/*
 * num_samples needs to be a perfect square !
 */

void			sampler_reg_generate_samples(t_sampler *sampler)
{
	int		index;
	int		j;
	int		p;
	int		q;
	int		n;

	n = sqrt(sampler->num_samples);
	index = -1;
	j = -1;
	while (++j < sampler->num_sets)
	{
		p = -1;
		while (++p < n)
		{
			q = -1;
			while (++q < n)
			{
				sampler->samples[++index].x = (q + 0.5) / n;
				sampler->samples[index].y = (p + 0.5) / n;
			}
		}
	}
}