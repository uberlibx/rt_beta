#include "rt.h"

void			sampler_pr_generate_samples(t_sampler *sampler)
{
	int		index;
	int		j;
	int		k;

	index = -1;
	j = -1;
	while (++j < sampler->num_sets)
	{
		k = -1;
		while (++k < sampler->num_samples)
		{
			sampler->samples[++index].x = rand_dbl();
			sampler->samples[index].y = rand_dbl();
		}
	}
}
