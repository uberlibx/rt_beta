#include "rt.h"

void			setup_shuffled_indices(t_sampler *sampler)
{
	int		j;
	int		p;
	int		*indices;
	int		index;

	indices = (int*)malloc(sizeof(int) * sampler->num_samples);

	index = -1;
	j = -1;
	while (++j < sampler->num_samples)
		indices[j] = j;

	p = -1;
	while (++p < sampler->num_sets)
	{
		random_shuffle(indices, 0, sampler->num_samples);
		j = -1;
		while (++j < sampler->num_samples)
			sampler->shuffled_indices[++index] = indices[j];
	}
	free(indices);
}
