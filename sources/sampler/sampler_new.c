#include "rt.h"

t_sampler	*sampler_new(int num_sets, int num_samples, int type)
{
	t_sampler	*sampler;

	sampler = (t_sampler*)malloc(sizeof(t_sampler));
	sampler->num_sets = num_sets;
	sampler->num_samples = num_samples;
	sampler->size = num_sets * num_samples;
	sampler->samples = (t_p2d*)malloc(sizeof(t_p2d) * sampler->size);
	sampler->shuffled_indices = (int*)malloc(sizeof(int) * sampler->size);
	sampler->count = 0;
	sampler->jump = 0;
	setup_shuffled_indices(sampler);
	shuffle_samples(sampler, type);
	return (sampler);
}
