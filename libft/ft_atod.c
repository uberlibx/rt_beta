#include <libft.h>
#include <math.h>

double	ft_atod(char *str)
{
	double	num;
	double	sign;
	int		i;
	int		j;

	num = ft_atoi(str);
	sign = (num < 0) ? -1 : 1;
	i = 0;
	j = 1;
	while (str[i] && str[i] != '.')
		i++;
	if (str[i])
		while(ft_isdigit(str[++i]))
			num += sign * ((str[i] - 48) / pow(10.0, j++));
	return (num);
}
