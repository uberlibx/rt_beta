NAME		= rt

MLX_PATH 	= /usr/local
IMLX_PATH	= ./imlx
LFT_PATH	= ./libft

BRDF_PATH		= ./sources/brdf
CAMERA_PATH		= ./sources/camera
COLOR_PATH		= ./sources/color
OB_PATH			= ./sources/ob
RAY_PATH		= ./sources/ray
LIGHT_PATH		= ./sources/light
MAT_PATH		= ./sources/material
MATHX_PATH		= ./sources/mathx
SAMP_PATH		= ./sources/sampler
SHADEREC_PATH	= ./sources/shaderec
TRACER_PATH		= ./sources/tracer
WORLD_PATH		= ./sources/world

SRC_4_OBJ	= *.c

SRC				= ./sources/*.c
SRC				+= $(BRDF_PATH)/$(SRC_4_OBJ)
SRC				+= $(CAMERA_PATH)/$(SRC_4_OBJ)
SRC				+= $(COLOR_PATH)/$(SRC_4_OBJ)
SRC				+= $(OB_PATH)/$(SRC_4_OBJ)
SRC				+= $(RAY_PATH)/$(SRC_4_OBJ)
SRC				+= $(LIGHT_PATH)/$(SRC_4_OBJ)
SRC				+= $(MAT_PATH)/$(SRC_4_OBJ)
SRC				+= $(MATHX_PATH)/$(SRC_4_OBJ)
SRC				+= $(SAMP_PATH)/$(SRC_4_OBJ)
SRC				+= $(SHADEREC_PATH)/$(SRC_4_OBJ)
SRC				+= $(TRACER_PATH)/$(SRC_4_OBJ)
SRC				+= $(WORLD_PATH)/$(SRC_4_OBJ)

OBJ				= ./sources/$(SRC_4_OBJ:.c=.o)

INCLUDES	= -I ./_includes
INCLUDES	+= -I $(MLX_PATH)/includes
INCLUDES	+= -I $(IMLX_PATH)/includes
INCLUDES	+= -I $(LFT_PATH)/includes

LIBS		= -lpthread
LIBS		+= -L $(MLX_PATH)/lib -lmlx
LIBS		+= -L $(IMLX_PATH) -limlx
LIBS		+= -L $(LFT_PATH) -lft

FRMW		= -framework OpenGL -framework AppKit
#FRMW		= -lXext -lX11 -lm

#FLAGS 		= -Wall -Werror -Wextra
FLAGS 		=

all: $(NAME)

$(NAME):
		@gcc -g $(FLAGS) $(INCLUDES) -c $(SRC)
		@mv *.o ./sources
		@gcc -o $(NAME) $(OBJ) $(FLAGS) $(LIBS) $(FRMW)

full:
		@make -C $(LFT_PATH)
		@make -C $(IMLX_PATH)
		@make

clean:
		@/bin/rm -f $(OBJ)

fclean: clean
		@/bin/rm -f $(NAME)

fullclean: fclean
		@make fclean -C $(LFT_PATH)
		@make fclean -C $(IMLX_PATH)

re: fclean all

fullre: fullclean full
