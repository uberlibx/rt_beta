#include "header.h"

void	error(int code)
{
	if (code == 0)
		ft_putendl("Error 0: Scene file could not be opened or created");
	else if (code == 1)
		ft_putendl("Error 1: Failed to read specified scene");
	else
		ft_putendl("Error code unknown, please contact developer");
	exit(1);
}

int	main(int ac, char **av)
{
	t_data	data;

	load_scene(&data, av[1]);
}
