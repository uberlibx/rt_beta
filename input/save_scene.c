#include "header.h"

void	save_scene(t_data *data)
{
	t_olist	*obj;
	t_llist	*light;
	int		fd;

	fd = open("temp", O_CREATE, O_WRONLY);
	obj = data->world.obj_list;
	while (obj)
	{
		fdprintf(fd, "[%i [%i [%i [%f,%f,%f [%f,%f,%f [%f [%i,%i\n",
		obj->object.id, obj->object.type, obj->object.material_id,
		obj->object.translation.x, obj->object.translation.y,
		obj->object.translation.z, obj->object.rotation.x,
		obj->object.rotation.y, obj->object.rotation.z, obj->object.radius,
		obj->object.cast_shadow, obj->object.receive_shadow);
		obj = obj->next;
	}
	light = data->world.light_list;
	while (light)
	{
		fdprintf(fd, "[%i [%i [0 [%f,%f,%f [%f,%f,%f [0 [%i,0\n",
		obj->object.id, obj->object.type, obj->object.translation.x,
		obj->object.translation.y, obj->object.translation.z,
		obj->object.rotation.x, obj->object.rotation.y, obj->object.rotation.z,
		obj->object.cast_shadow);
		light = light->next;
	}
}
