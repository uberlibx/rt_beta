#ifndef HEADER_H
# define HEADER_H

# include "../libft/includes/libft.h"
//# include "../imlx/includes/imlx.h"
//# include "../_includes/rt.h"
# include <stdio.h>
# include <dirent.h>
# include <fcntl.h>

typedef struct		s_trans
{
	double	x;
	double	y;
	double	z;
}					t_trans;

typedef struct		s_rot
{
	double	x;
	double	y;
	double	z;
}					t_rot;

typedef struct		s_ob
{
	int			type; // 0 - plane; 1 - sphere; 2 - disk
//	t_p3d		center;
//	t_nl		normal;
	t_trans		translation;
	t_rot		rotation;
	double		radius;
	double		k_epsilon;
	int			material_id;
//	t_material	*material;
	int			cast_shadow;
	int			receive_shadow;
	int			in_shadow;

//	t_color		color; // chapter 3
}					t_ob;

typedef struct		s_light
{
	/*
	** 0 - ambiental; 1 - directional; 2 - point
	*/
	int		type;
	t_trans		translation;
	t_rot		rotation;
	int		cast_shadow;
	double	ls; // light intensity
//	t_color	color; // light color

	/*
	** Directional
	*/
//	t_dir	dir; // direction of light

	/*
	** Point
	*/
//	t_p3d	location; // origin of light
}					t_light;

typedef struct			s_camera
{
//	t_p3d		eye;
//	t_p3d		look_at;
	t_trans		translation;
	t_rot		rotation;
	double		d; // distance to viewplane
//	t_dir		up;
//	t_dir		u;
//	t_dir		v;
//	t_dir		w;
	double		zoom;
	double		exposure_time;
}						t_camera;

typedef struct			s_olist
{
	t_ob	object;
	struct s_olist	*next;
}						t_olist;

typedef struct			s_llist
{
	t_light	light;
	struct s_llist	*next;
}						t_llist;

typedef struct			s_world
{
//	t_color		background_color;
//	t_color		(*trace_ray)(t_world *, t_ray *); // just the ray tracing function

//	t_sampler	*sampler;


	t_olist		*obj_list; // list of objects
	t_ob		*selected_ob;
	int			ob_arr_len; // remove maybe

	t_light		ambient; // ambiental light
	t_llist		*light_list; // list of lights
	t_light		*selected_light;
	int			light_arr_len; // remove maybe

	t_camera	camera;
}						t_world;

typedef struct	s_data
{
	char		*name;
	t_world		world;
	void		*mlx;
	void		*win;
	void		*img;
	int			*pixel;
	int			bpp;
	int			line;
	int			endian;
	int			is_render;
	double		to_rad;
}				t_data;

void	error(int code);
void	load_scene(t_data *data, char *name);
void	set_camera(t_world *world, double *ob);
void	add_object(t_world *world, double *ob);
void	add_light(t_world *world, double *ob);

#endif
