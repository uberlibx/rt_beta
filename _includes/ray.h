#ifndef RAY_H
# define RAY_H

# include "mathx.h"

struct		s_ray
{
	t_p3d	origin;
	t_dir	dir;
};

t_ray		ray_new(t_p3d origin, t_dir dir);

#endif
