#ifndef MATHX_H
# define MATHX_H

# define X_PI = 3.141592
# define I_PI = 0.318309

/*
** 2D Point
*/
struct			s_p2d
{
	double		x;
	double		y;
};

/*
** Point (3d)
*/
struct			s_p3d
{
	double	x;
	double	y;
	double	z;
};

/*
** Direction
*/
struct			s_dir
{
	double	x;
	double	y;
	double	z;
};

/*
** Normal
*/
struct			s_nl
{
	double	x;
	double	y;
	double	z;
};

/*
** 4x4 Matrix
*/
double			**matrix_new(); // identity matrix ??? UNUSED ???
void			matrix_identity(double (*m)[4][4]); // &m; m: double[4][4]
void			matrix_cpy(double src[4][4], double (*dest)[4][4]);
void			matrix_mult_matrix(double m1[4][4], double m2[4][4], double (*r)[4][4]);
void			matrix_mult_scalar(double (*m)[4][4], double d);
t_p3d			matrix_mult_p3d(double m[4][4], t_p3d p);
t_dir			matrix_mult_dir(double m[4][4], t_dir dir);
t_nl			matrix_mult_nl(double m[4][4], t_nl n);
void			matrix_print(double m[4][4]); // [TODEL] - for testing only, using printf

/*
** TRANSFORMATIONS
*/
void			tr_translate(double (*m)[4][4], double tx, double ty, double tz);
void			tr_rotate(double (*m)[4][4], double rx, double ry, double rz);

/*
** INVERSE TRANSFORMATIONS
*/
void			itr_translate(double (*m)[4][4], double tx, double ty, double tz);
void			itr_rotate(double (*m)[4][4], double rx, double ry, double rz);

/*
** RANDOM
*/
void			rand_set(const unsigned int seed);
double			rand_dbl(void);
double			rand_dbl_range(double min, double max);
int				rand_int(void);
int				rand_int_range(int min, int max);

/*
** 3D POINT
*/
t_p3d			p3d_new(double x, double y, double z);
t_p3d			p3d_add_dir(t_p3d a, t_dir b);
t_p3d			p3d_sub_dir(t_p3d a, t_dir b);
t_p3d			p3d_mult_scalar(t_p3d a, double n);
//double			p3d_dist_btwn_2(t_p3d a, t_p3d b);
double			dist_btwn_2_p3d(t_p3d a, t_p3d b);

/*
** DIRECTION
*/
void			dir_normalize(t_dir *v3);
t_dir			dir_new(double x, double y, double z);
t_dir			dir_add_dir(t_dir a, t_dir b);
t_dir			dir_cross_dir(t_dir a, t_dir b);
t_dir			dir_mult_scalar(t_dir a, double d);
t_dir			dir_sub_dir(t_dir a, t_dir b);
t_dir			dir_btwn_2_p3d(t_p3d a, t_p3d b);
t_dir			dir_neg(t_dir a);
double			dir_dot_dir(t_dir a, t_dir b);
double			dir_magnitude(t_dir a);

/*
** NORMAL
*/
void			nl_normalize(t_nl *a);
t_nl			nl_new(double x, double y, double z);
t_nl			nl_add_dir(t_nl a, t_dir b);
t_nl			nl_mult_scalar(t_nl a, double d);
t_nl			nl_from_dir(t_dir a);
t_nl			nl_from_p3d(t_p3d a);
double			nl_dot_dir(t_nl a, t_dir b);
double			nl_magnitude(t_nl a);

#endif
