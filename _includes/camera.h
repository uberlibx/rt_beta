#ifndef CAMERA_H
# define CAMERA_H

struct			s_camera
{
	t_p3d		eye;
	double		v_rotate[3]; // [TODO] switch to t_v3
	t_p3d		look_at;
	double		d; // distance to viewplane
	t_dir		up;
	t_dir		u;
	t_dir		v;
	t_dir		w;
	double		zoom;
	double		exposure_time;
};

void			set_camera(t_world *world, double *params);
t_camera		camera_new(t_p3d eye, t_p3d look_at, double d);
void			compute_uvw(t_camera *camera);
t_dir			ray_direction(t_camera *camera, t_p2d p);

#endif
