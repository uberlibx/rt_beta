/*
** [..]_f returns the brdf
** [..]_sample_f computes the direction of reflected rays
** [..]_rho returns the bihemispherical reflectance
*/

#ifndef BRDF_H
# define BRDF_H

#include "color.h"
#include "mathx.h"
#include "shaderec.h"
#include "sampler.h"

struct		s_brdf
{
	int			type; // 0 - lambertian; 1 - specular; 2 - glossy
	t_sampler	*sampler;
	t_nl		normal;
	/*
	** Diffuse
	*/
	t_color		cd; // diffuse color
	double		kd; // diffuse reflection coefficient [0 .. 1]

	/*
	** ---------- SPECULAR TYPES (one or the other)
	*/
	/*
	** Specular
	*/
	t_color		cr;
	double		kr; // [0 .. 1]
	/*
	** Glossy-Specular
	*/
	t_color		cs;
	double		ks; // [0 .. 1]
	double		exp; // [0 .. inf]
};

t_brdf		*brdf_new(int type);
t_brdf		*brdf_new_lamb(t_color color, double kd);
t_brdf		*brdf_new_spec(t_color color, double kr);
t_brdf		*brdf_new_glos(t_color color, double ks, double exp);

/*
** [TODO] Common - will call specific function, based on type
*/
t_color		brdf_f(t_brdf *brdf, t_shaderec *sr, t_dir *wi, t_dir wo);
t_color		brdf_sample_f(t_brdf *brdf, t_shaderec *sr, t_dir *wi, t_dir wo);
t_color		brdf_rho(t_brdf *brdf, t_shaderec *sr, t_dir wo);

/*
** Lambertian (matte)
*/
t_color		lamb_f(t_brdf *brdf, t_shaderec *sr, t_dir *wi, t_dir wo);
//t_color		lamb_sample_f(t_brdf *brdf, t_shaderec *sr, t_dir *wi, t_dir wo);
t_color		lamb_rho(t_brdf *brdf, t_shaderec *sr, t_dir wo);

/*
** Specular
*/
/*
**t_color		spec_f(t_brdf *brdf, t_shaderec *sr, t_dir *wi, t_dir wo);
**t_color		spec_sample_f(t_brdf *brdf, t_shaderec *sr, t_dir *wi, t_dir wo);
**t_color		spec_rho(t_brdf *brdf, t_shaderec *sr, t_dir wo);
*/

/*
** Glossy
*/
t_color		glos_f(t_brdf *brdf, t_shaderec *sr, t_dir *wi, t_dir wo);
t_color		glos_sample_f(t_brdf *brdf, t_shaderec *sr, t_dir *wi, t_dir wo);
//t_color		glos_rho(t_brdf *brdf, t_shaderec *sr, t_dir wo);

#endif
