#ifndef WORLD_H
# define WORLD_H

# include "tracer.h"
# include "sampler.h"
# include "camera.h"
# include "ob.h"
# include "light.h"
# include "material.h"

struct			s_olist
{
	t_ob	object;
	t_olist	*next;
};

struct			s_llist
{
	t_light	light;
	t_llist	*next;
};

struct			s_mlist
{
	t_material	material;
	t_mlist		*next;
};

struct			s_world
{
	t_color		background_color;
	t_color		(*trace_ray)(t_world *, t_ray *); // just the ray tracing function

	t_sampler	*sampler;


	t_olist		*obj_list; // list of objects
	t_ob		*selected_ob;
	t_mlist		*default_materials;
	t_mlist		*custom_materials;
	int			ob_arr_len; // remove maybe

	t_light		ambient; // ambiental light
	t_llist		*light_list; // list of lights
	t_light		*selected_light;
	int			light_arr_len; // remove maybe

	t_camera	camera;
};

t_world			world_new();
void			world_build_single_sphere(t_world *world);
void			world_build_bare_bones(t_world *world);
void			world_build(t_world *world);

/*
** Scene
*/
void			load_scene(t_data *data, char *file);
void			load_materials(t_data *data);


/*
** Tracers
*/
t_color			trace_ray_single_sphere(t_world *world, t_ray *ray);

t_shaderec		*hit_bare_bones(t_world *world, t_ray *ray);
t_color			trace_ray_bare_bones(t_world *world, t_ray *ray);

t_shaderec		hit_objects(t_world *world, t_ray *ray);
t_color			trace_ray(t_world *world, t_ray *ray);

int				hit_shadow(t_world *world, t_ray *ray, t_light *light, t_ob *hit);

#endif
