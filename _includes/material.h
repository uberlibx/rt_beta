#ifndef MATERIAL_H
# define MATERIAL_H

struct			s_material
{
	int		id;
	int		type; // type of shader; 0 - matte; 1 - phong
	int		source; // 0 - default; 1 - custom
	t_brdf	*ambient;
	t_brdf	*diffuse;
	t_brdf	*specular;
	t_world	*world;
};

void			add_material(t_world *world, double *params);

t_material		material_new(double *params);
t_material		*material_new_matte(t_color color, float ka, float kd);
t_material		*material_new_phong(t_color color, float ka, float kd, float ks);

t_color			material_shade(t_shaderec *sr);
t_color			material_whitted_shade(t_shaderec *sr); // [TODO]
t_color			material_area_light_shade(t_shaderec *sr); // [TODO]
t_color			material_path_shade(t_shaderec *sr); // [TODO]

t_color			shade_matte(t_shaderec *sr);
t_color			shade_phong(t_shaderec *sr);

#endif
