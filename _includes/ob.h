#ifndef OB_H
# define OB_H

# include "shaderec.h"
# include "color.h"
# include "mathx.h"
# include "world.h"
# include "material.h"

struct		s_ob
{
	int			id;
	int			type; // 0 - plane; 1 - sphere; 2 - disk
	t_p3d		center;
	t_nl		normal;
	double		radius;
	double		k_epsilon;
	int			material_id;
	t_material	material;
	int			cast_shadow;
	int			receive_shadow;
	int			in_shadow;

	// cylinder limits
	double		y0;
	double		y1;

	// cylinder && cone height
	double		height;

	// All transforms must be init to identity
	// Transformation matrix
	double		tr_translate[4][4];
	double		tr_rotate[4][4];
	double		transform[4][4]; // final transform

	// Inverse transformation
	double		itr_translate[4][4];
	double		itr_rotate[4][4];
	double		inverse_transform[4][4]; // final inverse transform

	t_color		color; // chapter 3
};

void		add_object(t_world *world, double *params);

t_ob		ob_new(double *params);
//t_ob		*ob_new_plane(t_p3d center, t_nl normal);
//t_ob		*ob_new_sphere(t_p3d center, double radius);
//t_ob		*ob_new_disk(t_p3d center, t_nl normal, double radius);

int			ob_hit(t_ob *ob, t_ray *ray, double *tmin, t_shaderec *sr);
int			ob_hit_plane(t_ob *ob, t_ray *ray, double *tmin, t_shaderec *sr);
int			ob_hit_sphere(t_ob *ob, t_ray *ray, double *tmin, t_shaderec *sr);
int			ob_hit_disk(t_ob *ob, t_ray *ray, double *tmin, t_shaderec *sr);
int			ob_hit_cylinder(t_ob *ob, t_ray *ray, double *tmin, t_shaderec *sr);
int			ob_hit_cone(t_ob *ob, t_ray *ray, double *tmin, t_shaderec *sr);

int			ob_shadow_hit(t_ob *ob, t_ray *ray, double *tmin);
int			ob_shadow_hit_plane(t_ob *ob, t_ray *ray, double *tmin);
int			ob_shadow_hit_sphere(t_ob *ob, t_ray *ray, double *tmin);
int			ob_shadow_hit_cylinder(t_ob *ob, t_ray *ray, double *tmin);
int			ob_shadow_hit_cone(t_ob *ob, t_ray *ray, double *tmin);

void		ob_translate(t_ob *ob, double dx, double dy, double dz);
void		ob_rotate(t_ob *ob, double rx, double ry, double rz);

#endif
