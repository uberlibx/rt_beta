#ifndef LIGHT_H
# define LIGHT_H

# include "color.h"
# include "mathx.h"
# include "shaderec.h"

struct		s_light
{
	int		id;
	/*
	** 0 - ambiental; 1 - directional; 2 - point
	*/
	int		type;
	int		cast_shadow;
	double	ls; // light intensity
	t_color	color; // light color
	double	tr_rotate[4][4];

	/*
	** Directional
	*/
	t_p3d	v_rotate; // [TODO] switch to t_v3
	t_dir	dir; // direction of light

	/*
	** Point
	*/
	t_p3d	location; // origin of light
};

void		add_light(t_world *world, double *params);

t_light		light_new(double *params);
t_light		light_new_amb(t_color color);
t_light		light_new_dir(t_color color, t_dir dir);
t_light		light_new_point(t_color color, t_p3d location);

t_dir		light_get_direction(t_light *light, t_shaderec *shaderec);
t_color		light_l(t_light *light, t_shaderec *shaderec); // ??? shaderec is unused

#endif
