#ifndef COLOR_H
# define COLOR_H

struct			s_color
{
	int			color;
	double		r;
	double		g;
	double		b;
};

t_color			color_new(float r, float g, float b);
t_color			color_add_color(t_color a, t_color b);
t_color			color_mult_color(t_color a, t_color b);
t_color			color_mult_scalar(t_color a, double b);
t_color			color_div_scalar(t_color a, double b);
t_color			color_pow(t_color a, double b);
t_color			color_max_to_one(t_color a); // if a component is > 1, divide all components by that value
t_color			color_clamp_to_color(t_color a); // if a component is > 1, set color to red
float			color_avg(t_color a);
void			build_color(t_color *a); // build the int component from doubles
void			build_color_raw(t_color *a);

#endif
