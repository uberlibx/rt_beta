#ifndef SAMPLER_H
# define SAMPLER_H

# include "mathx.h"
# include "mathx.h"

struct			s_sampler
{
	int		num_sets; // number of sets
	int		num_samples; // number of samples in a set
	int		size; // num_sets * num_sampler
	t_p2d	*samples; // array of 2d square samples
	t_p2d	*disk_samples; // array of 2d disc samples
	t_p3d	*hemisphere_samples; // array of 2d square samples
	int		*shuffled_indices; // shuffled samples array indices
	int		count; // the current number of sample points used
	int		jump; // random index jump
};

void			random_shuffle(int *arr, int first, int last); // [k]

t_sampler		*sampler_new(int num_sets, int num_samples, int type); // [k

void			setup_shuffled_indices(t_sampler *sampler); // [k] set up the randomly shuffled indices
void			shuffle_samples(t_sampler *sampler, int type); // [k] randomply shuffle the sample in each pattern, based on type

void			map_samples_to_unit_disk(t_sampler *sampler);
void			map_samples_to_hemisphere(t_sampler *sampler, double exp); // exp = 0 ... inf

t_p2d			sample_unit_square(t_sampler *sampler); // [k] get next sample on unit square
t_p2d			sample_unit_disk(t_sampler *sampler); // get next sample on unit disk
t_p3d			sample_hemisphere(t_sampler *sampler); // get next sample on hemisphere

void			shuffle_x_coordinates(t_sampler *sampler);
void			shuffle_y_coordinates(t_sampler *sampler);

/*
** "Class" specific sampler functions - generate sample patterns in a unit square
*/
void			sampler_reg_generate_samples(t_sampler *sampler); // [k] type 0
void			sampler_pr_generate_samples(t_sampler *sampler); // [k] type 1
void			sampler_jit_generate_samples(t_sampler *sampler); // [k] type 2
void			sampler_nrk_generate_samples(t_sampler *sampler); // [k] type 3
void			sampler_mjit_generate_samples(t_sampler *sampler); // type 4
void			sampler_ham_generate_samples(t_sampler *sampler); // [k] type 5

#endif
