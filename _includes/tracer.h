#ifndef TRACER_H
# define TRACER_H

# include "world.h"
# include "color.h"
# include "ray.h"

/*
** DECLARED IN world.h
**t_color			trace_ray_single_sphere(t_world *world, t_ray *ray);
**t_color			trace_ray_bare_bones(t_world *world, t_ray *ray);
**t_color			trace_ray(t_world *world, t_ray *ray);
*/

#endif
