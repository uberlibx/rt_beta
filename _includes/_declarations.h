#ifndef DECLARATIONS_H
# define DECLARATIONS_H

typedef struct s_data		t_data;

/*
** THREAD
*/

typedef struct s_thread		t_thread;

/*
** MATHX
*/

typedef struct s_p2d		t_p2d;
typedef struct s_p3d		t_p3d;
typedef struct s_dir		t_dir;
typedef struct s_nl			t_nl;

/*
** BRDF
*/

typedef struct s_brdf		t_brdf;

/*
** CAMERA
*/

typedef struct s_camera		t_camera;

/*
** COLOR
*/

typedef struct s_color		t_color;

/*
** LIGHT
*/

typedef struct s_light		t_light;

/*
** OB
*/

typedef struct s_ob			t_ob;

/*
** MATERIAL
*/

typedef struct s_material	t_material;
typedef struct s_mlist		t_mlist;

/*
** RAY
*/

typedef struct s_ray		t_ray;

/*
** SAMPLER
*/

typedef struct s_sampler	t_sampler;

/*
** SHADEREC
*/

typedef struct s_shaderec	t_shaderec;

/*
** TRACER
*/

typedef struct s_tracer		t_tracer;

/*
** WORLD
*/

typedef struct s_olist		t_olist;
typedef struct s_llist		t_llist;
typedef struct s_world		t_world;

#endif
