#ifndef RT_H
# define RT_H

// !!! [TO REMOVE] !!!
# include <unistd.h>
# include <stdio.h>
// ---

# include <stdlib.h>
# include <pthread.h>
# include <math.h>
# include <fcntl.h>
# include <mlx.h>
# include <imlx.h>
# include <libft.h>

# include "_declarations.h"

# include "color.h"
# include "mathx.h"
# include "ray.h"
# include "sampler.h"
# include "shaderec.h"
# include "ob.h"
# include "tracer.h"
# include "world.h"
# include "brdf.h"
# include "light.h"

# define THREAD_NR 40
# define TO_RAD 0.01745329251
# define INV_PI 0.3183099524
# define CAM_ANGLE 3.0
# define EYE_Z 300.0
# define DIST_VP 400.0
# define WIDTH 800
# define HEIGHT 600
# define TITLE "UBER RAY TRACER"
# define HUGE_VALUE 1000000.0
# define PIXEL_SIZE 1
# define GAMMA 1.0
# define INV_GAMMA 1.0
# define SETS 83
# define SAMPLES 1

struct			s_thread
{
	pthread_t	id;
	int			nr;
	int			start_h;
	int			end_h;
	t_data		*data;
};

struct			s_data
{
	char		*name;
	t_world		world;
	void		*mlx;
	void		*win;
	void		*img;
	int			*pixel;
	int			bpp;
	int			line;
	int			endian;
	int			is_render;
	t_thread	*thread;
};

void			error(int code);
void			data_init(t_data *data);
int				event_render(t_data *data);
int				event_keyboard(int k, t_data *data);

/*
** Find them in ./world :)
*/
/*
** Simple - Orthogonal
*/
void			render_scene_ortho(t_data *data);
void			render_scene_ortho_simple_ms(t_data *data);
void			render_scene_ortho_random_ms(t_data *data);
void			render_scene_ortho_jittered_ms(t_data *data);
void			render_scene_ortho_with_sampler(t_data *data);

/*
** Simple - Perspective
*/
void			render_scene_persp_no_camera(t_data *data); // no multi-sampling

/*
** Camera - Perspective
*/
void			render_scene_pinhole_camera(t_data *data);

/*
** Multithread
*/
void			*render_scene_pinhole_camera_multithread(void *thread_void);

#endif
