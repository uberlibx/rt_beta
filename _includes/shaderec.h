#ifndef SHADEREC_H
# define SHADEREC_H

# include "mathx.h"
# include "world.h"
# include "color.h"

struct			s_shaderec
{
	int			is_hit;
	t_ob		*hit_object;
	t_p3d		hit_point; // of the closest hit point
	t_p3d		local_hit_point; // of the closest hit point
	t_nl		normal; // of the closest hit point
	t_material	*material; // material of the closest hit object
	t_ray		*ray; // used for specular highlights
	double		t; // distance to closest object
	t_world		*world;
	t_color		color; // chapter 3
	int			depth; // for recursion depth ??? unused ???
	t_dir		dir; // for area lights ??? unused ???
};

t_shaderec		shaderec_new(t_world *world);

#endif
