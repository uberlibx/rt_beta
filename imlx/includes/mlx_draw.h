/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_draw.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgrigore <rgrigore@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/11 15:36:16 by rgrigore          #+#    #+#             */
/*   Updated: 2018/01/11 15:36:18 by rgrigore         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MLX_DRAW_H
# define MLX_DRAW_H

# ifdef __cplusplus
extern "C"{
# endif

# include <mlx.h>
# include <mlx_math.h>
# include <mlx_colors.h>

# define W_WIDTH 1200
# define W_HEIGHT 800
# define FOCAL_DIST 400

# define MLX_PITW mlx_put_image_to_window
# define MLX_XFTI mlx_xpm_file_to_image

typedef struct	s_img_data
{
	void	*value;
	int		l_size;
	int		*pixel;
	int		bpp;
	int		endi;
}				t_img_data;

typedef struct	s_vertex
{
	t_v3	local;
	t_v3	global;
	t_v3	camera;
	t_v2	projected;
	int		color;
}				t_vertex;

typedef struct	s_mesh
{
	t_vertex	*points;
	int			*lines;
	int			pct_count;
	int			line_count;
	t_mx4		translation;
	t_mx4		rotation;
	t_mx4		init_translation;
	t_mx4		init_rotation;
	double		width;
	double		depth;
	double		height_max;
	double		height_min;
}				t_mesh;

typedef struct	s_button
{
	int		id;
	t_v2	corner[4];
	void	(*target)(void*);
}				t_button;

typedef struct	s_menu
{
	t_button	*button;
	int			button_count;
	int			is_visible;
	t_img_data	gui;
}				t_menu;

typedef struct	s_gradient
{
	int	start;
	int	end;
}				t_gradient;

void			center_mesh(t_mesh *mesh);
void			mx_scale(t_mx4 *transform, double scale);
void			mx_rotate(t_mx4 *transform, double x, double y, double z);
void			mx_translate(t_mx4 *transform, double x, double y, double z);
void			to_perspective(t_v3 v3, t_v2 *v2);
void			to_ortho(t_v3 v3, t_v2 *v2, double zoom);
int				get_color(unsigned int start, unsigned int end, float position);
void			draw_img_line(int **px, t_vertex p1, t_vertex p2, double **z_b);

# ifdef __cplusplus
}
# endif

#endif
