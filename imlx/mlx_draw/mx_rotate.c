/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mx_rotate.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgrigore <rgrigore@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/26 01:36:24 by rgrigore          #+#    #+#             */
/*   Updated: 2018/01/26 01:36:27 by rgrigore         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx_draw.h>

static t_mx4	mx_rot_x(float alpha)
{
	t_mx4	result;

	mx_new(&result);
	result.y[1] = cos(alpha * 0.01745329252);
	result.z[1] = -sin(alpha * 0.01745329252);
	result.y[2] = sin(alpha * 0.01745329252);
	result.z[2] = cos(alpha * 0.01745329252);
	return (result);
}

static t_mx4	mx_rot_y(float alpha)
{
	t_mx4	result;

	mx_new(&result);
	result.x[0] = cos(alpha * 0.01745329252);
	result.z[0] = sin(alpha * 0.01745329252);
	result.x[2] = -sin(alpha * 0.01745329252);
	result.z[2] = cos(alpha * 0.01745329252);
	return (result);
}

static t_mx4	mx_rot_z(float alpha)
{
	t_mx4	result;

	mx_new(&result);
	result.x[0] = cos(alpha * 0.01745329252);
	result.y[0] = -sin(alpha * 0.01745329252);
	result.x[1] = sin(alpha * 0.01745329252);
	result.y[1] = cos(alpha * 0.01745329252);
	return (result);
}

void			mx_rotate(t_mx4 *transform, double x, double y, double z)
{
	*transform = mx_mult_4x4(*transform, mx_rot_x(x));
	*transform = mx_mult_4x4(*transform, mx_rot_y(y));
	*transform = mx_mult_4x4(*transform, mx_rot_z(z));
}
