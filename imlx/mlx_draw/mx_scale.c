/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mx_scale.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgrigore <rgrigore@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/26 11:37:33 by rgrigore          #+#    #+#             */
/*   Updated: 2018/01/26 11:37:36 by rgrigore         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx_draw.h>

void		mx_scale(t_mx4 *transform, double scale)
{
	t_mx4	scale_mx;

	mx_new(&scale_mx);
	scale_mx.x[0] = scale;
	scale_mx.x[1] = scale;
	scale_mx.x[2] = scale;
	*transform = mx_mult_4x4(*transform, scale_mx);
}
